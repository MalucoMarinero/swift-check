///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/sinon.d.ts"/>
///<reference path="../typedefs/chai.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>

var Promise: Bluebird.PromiseStatic = require('bluebird');

import Prop = require("../src/Properties");
import Gen = require("../src/Generators");
import Command = require("../src/Commands");
declare var describe, it, before; // Mocha injections.
var expect = require("chai").expect;
var sinon = require("sinon");
var _ = require('lodash');


describe("Commands Property Setup", () => {
    it("can create command property with Prop.commands()", () => {
        var commandsProp = Prop.commands(function() {
        });
    });

    it("setup a function with initial state using `this`", () => {
        var commandsProp = Prop.commands(function() {
            this.initialState(() => {
            });
        });
    });

    it("create commands via `this.command` in the setup function", () => {
        var commandsProp = Prop.commands(function() {
            this.addCommand("CommandName", (state) => {
                // do action
            }, (state) => {
                // advance state
            });
        });
    });

    it("add post and preconditions to a command", () => {
        var commandsProp = Prop.commands(function() {
            var c = this.addCommand("CommandName", (state) => {
            }, (state) => {
            });
            c.preCondition((s) => s > 0);
            c.postCondition((s1, s2, r) => s2 == r);

            expect(c.postConditions).to.have.length(1);
            expect(c.preConditions).to.have.length(1);

            var c2 = this.addCommand("CommandName", (state) => {
            }, (state) => {
            }, {
                preConditions: [((s) => s > 0), ((s) => s % 1 == 0)],
                postConditions: [((s1, s2, r) => s2 == r)],
            });

            expect(c2.postConditions).to.have.length(1);
            expect(c2.preConditions).to.have.length(2);
        });
    });

    it("add invariants via `this.addInvariant` in the setup function", () => {
        var commandsProp = Prop.commands(function() {
            this.addInvariant((state, sut) => {
                return state >= 0;
            });

            this.invariant((state, sut) => {
                return state >= 0;
            });
        });
    });
});


class Counter {
    constructor(public num: number) {}
    inc(): Counter { this.num += 1; return this; }
    dec(): Counter { this.num -= 1; return this; }
    get(): Counter { this.num; return this; }
    reset(): Counter { this.num = 0; return this; }
}

class FaultyCounter extends Counter {
    dec(): FaultyCounter { this.num -= this.num > 4 ? 2 : 1; return this; }
    inc(): FaultyCounter { this.num += this.num < -4 ? 2 : 1; return this; }
}

describe("Commands Property Running", () => {

    it("should be able to run tests on a functioning class", () => {
        var commandProp = Prop.commands(function() {
            this.initialState(() => { return 0; });
            this.initialSUT(() => { return new Counter(0); });

            var inc = this.addCommand("Inc", (c) => c.inc(), (s) => s += 1);
            var dec = this.addCommand("Dec", (c) => c.dec(), (s) => s -= 1);
            var get = this.addCommand("Get", (c) => c.get(), (s) => s);
            get.post((s1, s2, r) => { return s1 == r.num });
        });

        var result = commandProp.run();
        expect(result.status).to.equal(Prop.Status.Passed);
    });

    var faultyCommandProp = Prop.commands(function() {
        var c = new FaultyCounter(0);

        this.initialState(() => { return 0; });
        this.initialSUT(() => { return new FaultyCounter(0); });

        var inc = this.addCommand("Inc", (c) => c.inc(), (s) => s += 1);
        var dec = this.addCommand("Dec", (c) => c.dec(), (s) => s -= 1);
        var get = this.addCommand("Get", (c) => c.get(), (s) => s);
        get.post((s1, s2, r) => s1 == r.num);
    });


    var faultyPromisedProp = Prop.commands(function() {
        var c = new FaultyCounter(0);

        this.promised(true);
        this.initialState(() => { return 0; });
        this.initialSUT(() => { return new FaultyCounter(0); });

        var inc = this.addCommand("Inc", (c, d) => d(c.inc()), (s) => s += 1);
        var dec = this.addCommand("Dec", (c, d) => d(c.dec()), (s) => s -= 1);
        var get = this.addCommand("Get", (c, d) => d(c.get()), (s) => s);
        get.post(function(s1, s2, r) { return s1 == r.num; });
    });
    
    it("should be able to run tests on a faulty class, returning fail", () => {
        var result = faultyCommandProp.run();
        expect(result.status).to.equal(Prop.Status.Failed);
    });

    it("should provide command steps in the failed case if fail", () => {
        var result = faultyCommandProp.run();
        expect(result.failCase.commandSequence).to.exist;
        expect(result.failCase.commandSequence.length).to.be.above(4);
    });

    it("should shrink the test to the minimum required steps", () => {
        var result = faultyCommandProp.run();
        expect(result.failCase.commandSequence).to.exist;
        expect(result.failCase.commandSequence.length).to.be.below(8);
    });

    it("should shrink a promised test to the minimum required steps", (done) => {
        faultyPromisedProp.run().then((result) => {
            expect(result.failCase.commandSequence).to.exist;
            expect(result.failCase.commandSequence.length).to.be.below(8);
            done();
        }).catch(done);
    });

    it("should identify the postCondition or invariant that failed", () => {
        var result = faultyCommandProp.run();
        expect(result.failCase.commandSequence).to.exist;
        expect(result.failCase.getLabels()[0]).to.contain("postCondition failed");
    });

    it("invariant which return false should fail a property", () => {
        var commandProp = Prop.commands(function() {
            this.initialState(() => { return 0; });
            this.initialSUT(() => { return new Counter(0); });
            this.invariant((s) => false);

            var get = this.addCommand("Get", (c) => c.get(), (s) => s);
            get.post((s1, s2, r) => { return s1 == r.num; });
        });

        var result = commandProp.run();
        expect(result.status).to.equal(Prop.Status.Failed);
    });

    var restrictiveCommandProp = Prop.commands(function() {
        this.initialState(() => { return 0; });
        this.initialSUT(() => { return new Counter(0); });

        var inc = this.addCommand("Inc", (c) => c.inc(), (s) => s += 1);
        var dec = this.addCommand("Dec", (c) => c.dec(), (s) => s -= 1);
        var get = this.addCommand("Get", (c) => c.get(), (s) => s);
    });

    it("should throw error if no commands", () => {
        var prop = Prop.commands(function() {
            this.initialState(() => { return 0; });
            this.initialSUT(() => { return new Counter(0); });
        });

        expect(() => prop.run()).to.throw(/no commands specified/);
    });

    it("should be undecided if preconditions too restrictive", () => {
        var prop = Prop.commands(function() {
            this.initialState(() => { return 0; });
            this.initialSUT(() => { return new Counter(0); });

            var inc = this.addCommand("Inc", (c) => c.inc(), (s) => s += 1);
            inc.pre((s) => false);
            var dec = this.addCommand("Dec", (c) => c.dec(), (s) => s -= 1);
            dec.pre((s) => false);
            var get = this.addCommand("Get", (c) => c.get(), (s) => s);
            get.pre((s) => false);
        });

        var result = prop.run();
        expect(result.status).to.equal(Prop.Status.Undecided);
    });

    it("should not run commands with restricive preconditions", () => {
        var ranInc = false;
        var prop = Prop.commands(function() {
            this.initialState(() => { return 0; });
            this.initialSUT(() => { return new Counter(0); });

            var inc = this.addCommand("Inc", (c) => {
                ranInc = true;
                return c.inc();
            }, (s) => s += 1);
            inc.pre((s) => false);
            var dec = this.addCommand("Dec", (c) => c.dec(), (s) => s -= 1);
            var get = this.addCommand("Get", (c) => c.get(), (s) => s);
        });

        var result = prop.run();
        expect(result.status).to.equal(Prop.Status.Passed);
        expect(ranInc).to.be.false;
    });
});
