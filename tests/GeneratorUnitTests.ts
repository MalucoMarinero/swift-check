///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/sinon.d.ts"/>
///<reference path="../typedefs/chai.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

import Gen = require("../src/Generators");
declare var describe, it, before; // Mocha injections.
var expect = require("chai").expect;
var sinon = require("sinon");
var _ = require('lodash');

var rng = Math.random;


describe("OneOfGenerator", () => {
    it("can be given arguments of possible values", () => {
        Gen.oneOf('A', 'O', 'E', 'I', 'U');
    });

    it("can be given an array of possible values", () => {
        Gen.oneOf(['A', 'O', 'E', 'I', 'U']);
    });

    it("arbitrary returns items at relatively equal distribution", () => {
        var items = ['A', 'O', 'E', 'I', 'U'];
        var gen = Gen.oneOf(items);

        var distribution = {};
        _.range(10000).forEach((ix) => {
            var vowel = gen.arbitrary(rng, 0);
            if (!distribution[vowel]) {
                distribution[vowel] = 0;
            }
            distribution[vowel]++;
        });

        _.values(distribution).forEach((total) => {
            expect(total).to.be.within(500, 3500); // casting wide net
        });
    });

    it("shrink merely returns failing variable", () => {
        var items = ['A', 'O', 'E', 'I', 'U'];
        var gen = Gen.oneOf(items);

        expect(gen.shrink('A')).to.deep.equal(['A']);
    });
});


describe("BaseGenerator", () => {
    it("able to limit any generator with suchThat", () => {
        var items = ['A', 'O', 'E', 'I', 'U'];
        var gen = Gen.oneOf(items).suchThat((val) => val != 'A');

        var values = _.range(1000).map((ix) => gen.arbitrary(rng, 0));

        expect(values).not.to.contain('A');
    });
    
    it("throw error if suchThat filters out all values", () => {
        var items = ['A', 'O', 'E', 'I', 'U'];
        var gen = Gen.oneOf(items).suchThat((val) => val == 'F');

        expect(() => gen.arbitrary(rng, 0)).to.throw(
            Error, /suchThat filter too restrictive/
        );
    });

    it("expect shrinks to respect suchThat", () => {
        var items = ['A', 'O', 'E', 'I', 'U'];
        var gen = Gen.oneOf(items).suchThat((val) => val != 'A');

        var shrinks = gen.shrink('O');

        expect(shrinks).not.to.contain('A');
    });
});


describe("FrequencyGenerator", () => {
    it("receives a list of arguments with frequency first", () => {
        Gen.frequency([5, 'A'], [2, 'O'], [6, 'E'], [1, 'I'], [12, 'U']);
    });

    it("receives a nested array with frequency first", () => {
        Gen.frequency([
            [5, 'A'], [2, 'O'], [6, 'E'], [1, 'I'], [12, 'U']
        ]);
    });

    it("arbitrary distributes with weighting", () => {
        var gen = Gen.frequency<string>([
            [96, 'A'], [1, 'O'], [1, 'E'], [1, 'I'], [1, 'U']
        ]);

        var distribution = {};
        _.range(10000).forEach((ix) => {
            var vowel = gen.arbitrary(rng, 0);
            if (!distribution[vowel]) {
                distribution[vowel] = 0;
            }
            distribution[vowel]++;
        });

        expect(distribution['A']).to.be.above(9000);
    });

    it("shrink merely returns failing variable", () => {
        var gen = Gen.frequency<string>([
            [96, 'A'], [1, 'O'], [1, 'E'], [1, 'I'], [1, 'U']
        ]);

        expect(gen.shrink('A')).to.deep.equal(['A']);
    });
});




describe("SizedGenerator", () => {
    it("receives a function that takes a size and rng", () => {
        var gen = Gen.sized((rng, size) => size);
    });

    it("returns a value suitable to the size given", () => {
        var gen = Gen.sized((rng, size) => size);

        expect(gen.arbitrary(rng, 55)).to.equal(55);
    });

    it("can shrink after generating values", () => {
        var gen = Gen.sized((rng, size) => size);
        gen.arbitrary(rng, 55)
        var shrinks = gen.shrink(55);
        expect(shrinks).to.have.length(30);
        expect(shrinks).to.contain(54);
        expect(shrinks).to.contain(25);
    });
});


describe("ChooseGenerator", () => {
    it("receives two variables as a range", () => {
        var gen = Gen.choose(10, 40);
    });

    it("throws error if unlike types", () => {
        expect(() => Gen.choose(10, "A")).to.throw(/unlike types/);
    });

    it("throws error if min greater than max", () => {
        expect(() => Gen.choose(10, -20)).to.throw(/min greater than max/);
    });

    it("generates number values between the range only", () => {
        var gen = Gen.choose(10, 40);

        _.range(5000).forEach((ix) => {
            expect(gen.arbitrary(rng, 100)).to.be.within(10, 40);
        });
    });

    it("shrinks to numbers within the range, and below the value given", () => {
        var gen = Gen.choose(10, 40);

        var shrinks = gen.shrink(35);

        shrinks.forEach((val) => {
            expect(val).to.be.within(10, 35);
        });
    });

    it("shrinks to char values within the range and below the given value", () => {
        var gen = Gen.choose('A', 'Z');

        var shrinks = gen.shrink('T');

        shrinks.forEach((val) => {
            expect(val).to.be.within('A', 'T');
        });
    });

    it("generates char values between the range only", () => {
        var gen = Gen.choose('A', 'Z');

        _.range(5000).forEach((ix) => {
            var val = gen.arbitrary(rng, 100);
            expect(val).to.be.within('A', 'Z');
        });
    });
    
    it("chooseInt generates integer values between the range only", () => {
        var gen = Gen.chooseInt(10, 40);

        _.range(5000).forEach((ix) => {
            var val = gen.arbitrary(rng, 100);
            expect(val === +val && val === (val|0)).to.be.true;
            expect(val).to.be.within(10, 40);
        });
    });
});


describe("MapGenerator", () => {
    it("receives a generator and a  function that maps a generator value", () => {
        Gen.map(Gen.chooseInt(0, 10), (val) => val * 100);
    });

    it("resulting values are mapped by the function", () => {
        var gen = Gen.map(
            Gen.chooseInt(30, 100), (val) => String.fromCharCode(val)
        );

        _.range(5000).forEach((ix) => {
            var val = gen.arbitrary(rng, 100);
            expect(typeof val).to.equal("string");
        });
    });
});


describe("ArrayGenerator", () => {
    it("receives a contents generator", () => {
        Gen.arrayOf(Gen.chooseInt(0, 100));
    });

    it("receives a generator and minimum size", () => {
        Gen.arrayOf(Gen.chooseInt(0, 100), 5);
    });

    it("throws an error if minimum size below 0", () => {
        expect(() => Gen.arrayOf(Gen.chooseInt(0, 100), -1)).to.throw(Error);
    });

    it("receives a generator and size range", () => {
        Gen.arrayOf(Gen.chooseInt(0, 100), 5, 20);
    });

    it("throws an error if max size below minimum", () => {
        expect(() => Gen.arrayOf(Gen.chooseInt(0, 100), 21, 5)).to.throw(Error);
    });
    
    it("generates arrays of the contents generated values", () => {
        var gen = Gen.arrayOf(Gen.chooseInt(0, 100));

        var val = gen.arbitrary(rng, 20);
        expect(_.isArray(val)).to.be.true;
    });

    it("shrunken arrays should all be same or smaller than given array", () => {
        var gen = Gen.arrayOf(Gen.chooseInt(0, 100));

        var shrinks = gen.shrink([5,4,3,34,231]);
        expect(shrinks.every((v) => v.length <= 5)).to.be.true;
    });
});


describe("Gen.Values", () => {
    it(".string() generates strings", () => {
        var gen = Gen.Values.string();
        expect(typeof gen.arbitrary(rng, 40)).to.equal('string');
    });

    it("arrayOf(string()) generates arrays of string", () => {
        var gen = Gen.arrayOf(Gen.Values.string());
        _.range(100).forEach((ix) => {
            var val = gen.arbitrary(rng, 100);
            expect(val).instanceof(Array);
            expect(val.every((x) => typeof x == 'string')).to.be.true;
        });
    });

    it(".boolean() generates boolean", () => {
        var gen = Gen.Values.boolean();

        _.range(100).forEach((ix) => {
            expect(typeof gen.arbitrary(rng, 40)).to.equal('boolean');
        });
    });

    it(".date() generates date", () => {
        var gen = Gen.Values.date();
        expect(gen.arbitrary(rng, 40)).to.be.instanceof(Date);
    });
});
