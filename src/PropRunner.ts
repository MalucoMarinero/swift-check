
///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var Promise: Bluebird.PromiseStatic = require('bluebird');
var _ = require('lodash');
var colors = require('colors');

import Prop = require('./Properties');


export class PropertyFailure extends Error {
    public name: string;
    public message: string;
    public failCase: Prop.TestCase;

    constructor(
        public result: Prop.Result, name?: string
    ) {
        this.failCase = result.failCase;

        this.name = "PropertyFailure";
        this.message = "";
        if (name) {
            this.message += name + ' ';
        }
        this.message += "PropertyFailure: Failed after " + result.passes;
        this.message += " tests.\n";

        if (this.failCase.getLabels().length > 0) {
            this.message += this.failCase.getLabels().map((label) => {
                return ":: " + label;
            }).join("\n") + "\n";
        };

        this.message += this.failCase.getArgs().map((arg, ix) => {
            return "> ARG " + ix + ": " + arg.toString();
        }).join("\n");
    }
}


export class PropertyUndecided extends Error {
    public name: string;
    public message: string;

    constructor(
        public result: Prop.Result, name?: string
    ) {
        this.name = "PropertyUndecided";
        this.message = "";
        if (name) {
            this.message += name + ' ';
        }
        this.message += "PropertyUndecided: Gave up after only ";
        this.message += result.passes + " passed tests. ";
        this.message += result.discards + " tests were discarded.";
    }
}


function reportSuccess(result: Prop.Result, name?: string) {
    if (name) {
        var output = "\n+ " + name + " OK, passed "
        output += result.passes + " tests.\n";
    } else {
        var output = "\n+ OK, passed " + result.passes + " tests.\n";
    }

    var collectedOutput = "";

    _.forOwn(result.testData, (num, name) => {
        var perc = Math.floor(num / result.passes * 100);
        collectedOutput += perc + "% " + name + "\n";
    });

    if (collectedOutput) {
        output += "> Collected test data:\n";
        output += collectedOutput;
    }

    console.log(output["green"]);
}


function processResult(result: Prop.Result, name?: string, silent?: boolean): Prop.Result {
    if (result.results) {
        _.forOwn(result.results, (res, key) => {
            var propName = name + '.' + key;
            switch (res.status) {
                case Prop.Status.Passed:
                if (!silent) {
                    reportSuccess(res, propName);
                }
                break;

                case Prop.Status.Undecided:
                throw new PropertyUndecided(res, propName);
                break;

                case Prop.Status.Failed:
                throw new PropertyFailure(res, propName);
                break;
            };
        });
    } else {
        switch (result.status) {
            case Prop.Status.Passed:
            if (!silent) {
                reportSuccess(result, name);
            }
            return result;

            case Prop.Status.Undecided:
            throw new PropertyUndecided(result, name);
            break;

            case Prop.Status.Failed:
            throw new PropertyFailure(result, name);
            break;
        }
    }

    return result;
}

export function checkProp(
    prop: Prop.Property, parameters?: Prop.RunParameters
): any {
    var result = prop.run(parameters);

    if (result['then']) {
        return result.then((res) => {
            return processResult(res, prop.name);
        });
    } else {
        return processResult(result, prop.name);
    }
}


export function check(
    prop: Prop.Property, parameters?: Prop.RunParameters
): any {
    var result = prop.run(parameters);

    if (result['then']) {
        return result.then((res) => {
            return processResult(res, prop.name, true);
        });
    } else {
        return processResult(result, prop.name, true);
    }

}
