# What is SwiftCheck

SwiftCheck is a tool for testing Javascript using property specifications and automatically generated test data. By using automatically generated test data you can ensure a greater coverage than you would achieve through manual writing of test examples.

# Installing

Install with:

    npm install swift-check

Require with:

    var SwiftCheck = require('swift-check');

# A Quick Example

Lets require in `SwiftCheck.Prop` and `SwiftCheck.Gen`

    var Prop = require('swift-check').Prop;
    var Gen = require('swift-check').Gen;

And define a property we know should hold. It needs to return true, false, or a promise of true or false.

    var propSplitJoinString = Prop.forAll(
        Gen.Values.string(), function(str) {
            return str.split("").join("") == s;
        }
    );

Now if we run `checkData()`

    propSplitJoinString.checkData();
    // + OK, passed 100 tests

That looks like it passed. Lets try a property that will fail.

    var propSquared = Prop.forAll(
        Gen.Values.tinyPositiveInt(), function(num) {
            return num * num === num;
        }
    );

Now if we run `checkData()`

    propSquared.checkData();
    // > PropertyFailure: Failed after 2 tests.
    // > ARG 0: 2

The property is falsified after two tests with the argument `2`. If you want to run properties as part of your testing suite, use prop.check() to have it succeed silently (no console out), or throw an exception on failure.

# More to Come

There's a lot to property based testing. The [Guide to ScalaCheck](https://github.com/rickynils/scalacheck/wiki/User-Guide) is an excellent guide to property based testing in general while I get my act together sorting out a complete line of documentation for SwiftCheck.

Features that are already supported include:

- building blocks for a variety of generators, including `choose`, `oneOf`, `frequency`, `arrayOf`, and `sized` for entirely custom generators.
- premade generators under `SwiftCheck.Gen.Values`
- implications by using `this.implies(true || false)` within property functions.
- labels by using `this.label(true || false, "if true", "if false")` within property functions -- display collected data by running `prop.checkData()`
- combining properties using `p1.and(p2)` and `p1.or(p2)`.

On the roadmap, look at ScalaCheck's feature set, cause that's where we'll be heading, Commands testing for state system, more features for property grouping, and expanding Generators where required.








MIT License

Copyright (c) 2013 Full and By Design

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

