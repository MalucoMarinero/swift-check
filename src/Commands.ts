///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var Promise: Bluebird.PromiseStatic = require('bluebird');
var _ = require('lodash');
var colors = require('colors');

import Gen = require('./Generators');
import Prop = require('./Properties');


export interface CommandSequenceStep {
    postState: any;
    stepName: any;
}

export class CommandTestCase extends Prop.TestCase {
    public commandSequence: CommandSequenceStep[];

    constructor() {
        super([]);
        this.commandSequence = [];
    }

    addStep(name: string, state: any) {
        this.commandSequence.push({
            stepName: name, postState: state
        });
    }
}

export class PreconditionsExhausted {
    public name: string;
    constructor(public message: string) {
        this.name = "PreconditionsExhausted";
    }
}

export class CommandsProperty extends Prop.BaseProperty {
    public commands: {[key: string]: Command};
    public setupFunc: () => any;
    public setupSUT: () => any;
    public invariants: Array<(state: any, sut: any) => boolean>;
    public isPromised: boolean;

    constructor(commandSetup: any) {
        super();
        this.commands = {};
        this.invariants = [];
        commandSetup.call(this, this);
    }

    initialState(setupFunc: () => any) {
        this.setupFunc = setupFunc;
    }

    initialSUT(setupFunc: () => any) {
        this.setupSUT = setupFunc;
    }

    promised(b: boolean) {
        this.isPromised = b;
    }


    addCommand(
        name: string,
        run?: (sut: any) => any, nextState?: (state: any) => void,
        extras?: any
    ): Command;
    addCommand(
        name: string,
        run?: (sut: any, callback?: any) => any, nextState?: (state: any) => void,
        extras?: any
    ): Command {
        var command = new Command(name, run, nextState);
        this.commands[name] = command;

        if (extras) {
            if (extras.pre || extras.preConditions) {
                var preConditions = extras.pre || extras.preConditions;
                if (_.isArray(preConditions)) {
                    preConditions.forEach((pre) => command.pre(pre));
                } else {
                    command.pre(preConditions);
                }
            }

            if (extras.post || extras.postConditions) {
                var postConditions = extras.post || extras.postConditions;
                if (_.isArray(postConditions)) {
                    postConditions.forEach((post) => command.post(post));
                } else {
                    command.post(postConditions);
                }
            }
        }

        return command;
    }

    invariant(invariant: (state, sut) => boolean) { this.addInvariant(invariant); }
    addInvariant(invariant: (state, sut) => boolean) {
        this.invariants.push(invariant);
    }


    shrinkTestCase(testCase: CommandTestCase, explore?: boolean): CommandTestCase {
        var maxLength = testCase.commandSequence.length;
        var commands = _.values(this.commands);

        if (!explore && maxLength > 7) maxLength = 7; 

        var possibleCombinations = Math.pow(commands.length, maxLength);

        var iterator = _.range(maxLength).map((xs) => -1);

        var tries = 0;
        var ceiling = 0;
        while (iterator.some((cIx, ix) => cIx < commands.length - 1)) {
            tries++;
            // advance first iterator
            iterator[0]++;
            for (var ix = 0; ix < iterator.length; ix++) {
                if (iterator[ix] == commands.length && ix == ceiling) {
                    ceiling++;
                }
                if (iterator[ix] == commands.length) {
                    iterator[ix] = 0;
                    if (ix + 1 < iterator.length) iterator[ix + 1]++;
                }
            }

            // run testCase
            var nextTestCase = new CommandTestCase();
            var state = this.setupFunc.apply(nextTestCase);
            var SUT = this.setupSUT.apply(nextTestCase);
            var result = true;

            for (var ix = 0; ix < ceiling + 1; ix++) {
                var nextCom = commands[iterator[ix]];

                var nextSUT = nextCom.run.apply(nextTestCase, [SUT]);
                var nextState = nextCom.nextState.apply(nextTestCase, [state]);
                nextTestCase.addStep(nextCom.name, nextState);

                nextCom.postConditions.forEach((post) => {
                    if (post.apply(testCase, [state, nextState, nextSUT])) {
                        return;
                    }
                    result = false;
                    nextTestCase.label("postCondition failed: " + post.toString());
                });

                this.invariants.forEach((inv) => {
                    if (inv.apply(nextTestCase, [nextState, nextSUT])) return;
                    result = false;
                    nextTestCase.label("invariant failed: " + inv.toString());
                });

                if (!result) {
                    break;
                } else {
                    SUT = nextSUT;
                    state = nextState;
                }
            }

            nextTestCase.args = [nextTestCase.commandSequence.map((c) => c.stepName)];
            if (!result) {
                nextTestCase.setResult(result);
                return nextTestCase
            }
        }

        // exhausted possibilities
        return testCase;
    }


    promiseShrinkTestCase(
        testCase: CommandTestCase, explore?: boolean
    ): Bluebird.Promise {
        var maxLength = testCase.commandSequence.length;
        var commands = _.values(this.commands);

        if (!explore && maxLength > 7) maxLength = 7; 

        var possibleCombinations = Math.pow(commands.length, maxLength);

        var iterator = _.range(maxLength).map((xs) => -1);

        var tries = 0;
        var sprintEnd = 2;
        var ceiling = 0;
        var iterations = [];
        var doSprint = () => {
        while (iterator.some((cIx, ix) => cIx < commands.length - 1)) {
            if (ceiling == sprintEnd) break;
            tries++;
            // advance first iterator
            iterator[0]++;
            for (var ix = 0; ix < iterator.length; ix++) {
                if (iterator[ix] == commands.length && ix == ceiling) {
                    ceiling++;
                }
                if (iterator[ix] == commands.length) {
                    iterator[ix] = 0;
                    if (ix + 1 < iterator.length) iterator[ix + 1]++;
                }
            }

            iterations.push(new Promise((resolve, reject) => {
                // run testCase
                var nextTestCase = new CommandTestCase();
                var state = this.setupFunc.apply(nextTestCase);
                var SUT = this.setupSUT.apply(nextTestCase);
                var result = true;

                var doStep = (ix, testState) => {
                    if (testState == false) return Promise.resolve(false);
                    var nextCom = commands[iterator[ix]];

                    if (nextCom.run.length == 2) {
                        // call with callback
                        var systemPromise = new Promise((res, rej) => {
                            var next = nextCom;
                            nextCom.run.apply(nextTestCase, [
                                testState.sut, function(newSUT) {
                                    res(newSUT);
                                }
                            ]);
                        });
                    } else {
                        var systemPromise = Promise.resolve(
                            nextCom.run.apply(nextTestCase, [testState.sut])
                        );
                    }

                    return systemPromise.then((nextSUT): any => {
                        var nextState = nextCom.nextState.apply(nextTestCase, [testState.state]);
                        nextTestCase.addStep(nextCom.name, nextState);

                        nextCom.postConditions.forEach((post) => {
                            if (post.apply(testCase, [testState.state, nextState, nextSUT])) {
                                return;
                            }
                            result = false;
                            testCase.label("postCondition failed: " + post.toString());
                        });

                        this.invariants.forEach((inv) => {
                            if (inv.apply(testCase, [nextState, nextSUT])) {
                                return;
                            }
                            result = false;
                            testCase.label("invariant failed: " + inv.toString());
                        });

                        if (!result) {
                            return false;
                        } else {
                            return {
                                sut: nextSUT,
                                state: nextState
                            };
                        }
                    });
                };

                var testPromise = Promise.resolve({
                    sut: SUT,
                    state: state,
                });

                for(var i = 0; i < ceiling + 1; i++) {
                    testPromise = testPromise.then(doStep.bind(undefined, i));
                };

                return resolve(testPromise.then((finalState) => {
                    // if (discard) continue;
                    nextTestCase.args = [nextTestCase.commandSequence.map((c) => c.stepName)];

                    nextTestCase.setResult(result);

                    return nextTestCase;
                }));

            }));

            return Promise.settle(iterations).then((resultSet) => {
                for (var ix = 0; ix < resultSet.length; ix++) {
                    var pi = resultSet[ix];
                    
                    if (pi.isRejected()) {
                        if (pi._settledValue.name == 'ImplicationFailure') {
                            continue;
                        } else {
                            throw pi._settledValue;
                        }
                    } else {
                        var nextTestCase = pi.value();
                    }

                    if (nextTestCase.getResult()) {
                        continue;
                    } else {
                        return nextTestCase; // failed case
                    }
                };

                // exhausted possibilities
                if (ceiling < maxLength) {
                    sprintEnd++;
                    return doSprint();
                } else {
                    return testCase;
                }
            });
        }; // END WHILE LOOP
        }; // END doSprint();

        return doSprint();
    }


    nextCommand(p: Prop.RunParameters, size: number, state: any): Command {
        var next: Command;
        var valid = false;
        var tries = 0;
        while (!valid) {
            tries++;
            if (tries > 10) {
                var fails = next.preConditions.filter((pre) => !pre(state))
                var failOutput = "Preconditions not satisfied after 50 tries.\n";
                failOutput += fails.map((f) => f.toString()).join("\n");
                valid = next.preConditions.every((pre) => pre(state));
                throw new PreconditionsExhausted(failOutput);
            }

            var commandGen = Gen.oneOf(_.values(this.commands));
            next = commandGen.arbitrary(p.rng, size);

            valid = next.preConditions.every((pre) => pre(state));
        }

        return next;
    }


    run(p?: Prop.RunParameters): any {
        if (_.values(this.commands).length == 0) {
            throw new Error("Property can't run: no commands specified");
        }

        p = p || {};
        _.defaults(p, Prop.DefaultRunParameters);

        if (this.isPromised) return this.runPromised(p);

        var stepSize = (p.maxSize-p.minSize) / p.minTestPasses;

        var results: CommandTestCase[] = [];
        var passes = 0;
        var discards = 0;
        var currentSize = 0;
        var collectedTestData = {};

        var discardRatioPassed = function() {
            return (discards + passes > p.minTestPasses &&
                    discards / p.maxDiscardRatio >= p.minTestPasses);
        }

        while (passes < p.minTestPasses) {
            if (discardRatioPassed()) { break; }

            var testCase = new CommandTestCase();
            var state = this.setupFunc.apply(testCase);
            var SUT = this.setupSUT.apply(testCase);
            var discard = false;
            var result = true;
            
            while(testCase.commandSequence.length < currentSize) {
                try {
                    var nextCom = this.nextCommand(p, currentSize, state);
                } catch (e) {
                    if (e.name == "PreconditionsExhausted") {
                        discards++;
                        currentSize += stepSize;
                        discard = true;
                        break;
                    } else {
                        throw e;
                    }
                }

                var nextSUT = nextCom.run.apply(testCase, [SUT]);
                var nextState = nextCom.nextState.apply(testCase ,[state]);
                testCase.addStep(nextCom.name, nextState);

                nextCom.postConditions.forEach((post) => {
                    if (post.apply(testCase, [state, nextState, nextSUT])) {
                        return;
                    };
                    result = false;
                    testCase.label("postCondition failed: " + post.toString());
                });

                this.invariants.forEach((inv) => {
                    if (inv.apply(testCase, [nextState, nextSUT])) {
                        return;
                    }
                    result = false;
                    testCase.label("invariant failed: " + inv.toString());
                });

                if (!result) {
                    break;
                } else {
                    SUT = nextSUT;
                    state = nextState;
                }
            }

            if (discard) continue;
            testCase.args = [testCase.commandSequence.map((c) => c.stepName)];

            testCase.setResult(result);
            results.push(testCase);

            if (!testCase.getResult()) {
                break;
            } else {
                currentSize += stepSize;
                passes++;
            }
        }

        // return result of testing
        if (discardRatioPassed()) {
            return new Prop.Result(
                Prop.Status.Undecided,
                passes, discards,
                collectedTestData
            );
        } else if (passes < p.minTestPasses) {
            var failCase = _.find(results, (res) => !res.result);
            var shrunkFailCase = this.shrinkTestCase(failCase, p.explore);

            return new Prop.Result(
                Prop.Status.Failed,
                passes, discards,
                collectedTestData,
                shrunkFailCase
            );
        } else {
            return new Prop.Result(
                Prop.Status.Passed,
                passes, discards,
                collectedTestData
            );
        }
    }


    runPromised(p?: Prop.RunParameters): any {
        var stepSize = (p.maxSize-p.minSize) / p.minTestPasses;

        var results: Bluebird.Promise[] = [];
        var sprintProgress = 0;
        var sprintEnd = p.minTestPasses;
        var passes = 0;
        var discards = 0;
        var currentSize = 0;
        var collectedTestData = {};

        var discardRatioPassed = function() {
            return (discards + passes > p.minTestPasses &&
                    discards / p.maxDiscardRatio >= p.minTestPasses);
        }

        var doSprint = () => {
        while (sprintProgress < sprintEnd) {
            if (discardRatioPassed()) { break; }

            results.push(new Promise((resolve, reject) => {

                var testCase = new CommandTestCase();
                var state = this.setupFunc.apply(testCase);
                var SUT = this.setupSUT.apply(testCase);
                var size = currentSize;
                var discard = false;
                var result = true;

                var doStep = (testState) => {
                    if (testState == false) return false;
                    try {
                        var nextCom = this.nextCommand(p, size, testState.state);
                    } catch (e) {
                        if (e.name == "PreconditionsExhausted") {
                            discards++;
                            size += stepSize;
                            discard = true;
                            return reject(e);
                        } else {
                            throw e;
                        }
                    }

                    if (nextCom.run.length == 2) {
                        // call with callback
                        var systemPromise = new Promise((res, rej) => {
                            var next = nextCom;
                            next.run.apply(testCase, [
                                testState.sut, function(newSUT) {
                                    res(newSUT);
                                }
                            ]);
                        });
                    } else {
                        var systemPromise = Promise.resolve(
                            nextCom.run.apply(testCase, [testState.sut])
                        );
                    }

                    return systemPromise.then((nextSUT): any => {

                        var nextState = nextCom.nextState.apply(testCase, [testState.state]);
                        testCase.addStep(nextCom.name, nextState);

                        nextCom.postConditions.forEach((post) => {
                            if (post.apply(testCase, [testState.state, nextState, nextSUT])) {
                                return;
                            }
                            result = false;
                            testCase.label("postCondition failed: " + post.toString());
                        });

                        this.invariants.forEach((inv) => {
                            if (inv.apply(testCase, [nextState, nextSUT])) {
                                return;
                            }
                            result = false;
                            testCase.label("invariant failed: " + inv.toString());
                        });

                        if (!result) {
                            return false;
                        } else {
                            return {
                                sut: nextSUT,
                                state: nextState
                            };
                        }
                    });
                };

                var testPromise = Promise.resolve({
                    sut: SUT,
                    state: state,
                });

                for(var i = 0; i < size; i++) {
                    testPromise = testPromise.then(doStep);
                };

                return resolve(testPromise.then((finalState) => {
                    // if (discard) continue;
                    testCase.args = [testCase.commandSequence.map((c) => c.stepName)];

                    testCase.setResult(result);

                    return testCase;
                }));
            }));

            currentSize += stepSize;
            sprintProgress++;

        };

        return Promise.settle(results).then((resultSet) => {
            var collectedTestData = {}
            var passes = 0;
            var discardRatioPassed = function() {
                return (discards + passes > p.minTestPasses &&
                        discards / p.maxDiscardRatio >= p.minTestPasses);
            }

            for (var ix = 0; ix < resultSet.length; ix++) {
                var pi = resultSet[ix];
                
                if (pi.isRejected()) {
                    if (pi._settledValue.name == 'PreconditionsExhausted') {
                        discards++;
                        continue;
                    } else {
                        throw pi._settledValue;
                    }
                } else {
                    var testCase = pi.value();
                }

                if (!testCase.getResult()) {
                    break;
                } else {
                    var classString = testCase.getClassificationString();
                    if (classString) {
                        if (_.isUndefined(collectedTestData[classString])) {
                            collectedTestData[classString] = 0
                        }
                        collectedTestData[classString]++;
                    }

                    passes++;
                }
            };

            var failCase = _.find(resultSet, (res) => {
                return res.isRejected() ? false : !res.value().result;
            });
            // return result of testing
            if (discardRatioPassed()) {
                return new Prop.Result(
                    Prop.Status.Undecided,
                    passes, discards,
                    collectedTestData
                );
            } else if (failCase) {
                // promised not shrinking for now
                // var shrunkFailCase = this.shrinkTestCase(failCase);
                var pR = this.promiseShrinkTestCase(failCase.value())
                return pR.then((shrunk) => {
                    return new Prop.Result(
                        Prop.Status.Failed,
                        passes, discards,
                        collectedTestData,
                        shrunk
                    );
                });
            } else if (passes >= p.minTestPasses) {
                return new Prop.Result(
                    Prop.Status.Passed,
                    passes, discards,
                    collectedTestData
                );
            } else {
                sprintEnd += p.minTestPasses;
                return doSprint();
            }
        });

        };

        return doSprint();

    }
}


export class Command {
    public preConditions: Array<(state: any) => boolean>;
    public postConditions: Array<(s1: any, s2: any, r: any) => boolean>;

    constructor(
        name: string,
        run?: (sut: any) => any,
        nextState?: (state: any) => void
    );
    constructor(
        public name: string,
        public run?: (sut: any, callback?: any) => any,
        public nextState?: (state: any) => void
    ) {
        this.preConditions = [];
        this.postConditions = [];
    }

    pre(pre: (state: any) => boolean) { this.preCondition(pre); }
    preCondition(pre: (state: any) => boolean) {
        this.preConditions.push(pre);
    }

    post(post: (s1: any, s2: any, r: any) => boolean) { this.postCondition(post); }
    postCondition(post: (s1: any, s2: any, r: any) => boolean) {
        this.postConditions.push(post);
    }
}

