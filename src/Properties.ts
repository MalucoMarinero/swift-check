///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var Promise: Bluebird.PromiseStatic = require('bluebird');
var _ = require('lodash');
var colors = require('colors');

import Gen = require('./Generators');
import PropRunner = require('./PropRunner');


export enum Status {
    Failed, Undecided, Passed
}


export class Result {
    constructor(
        public status: Status,
        public passes: number,
        public discards: number,
        public testData: any,
        public failCase?: TestCase,
        public results?: {[key: string]: Result}
    ) { 
    }

    combine(status: Status, results: Result[]): Result {
        var mergedData = _.clone(this.testData);
        var passes = this.passes;
        var discards = this.discards;
        results.forEach((result) => {
            passes += result.passes;
            discards += result.discards;

            _.forOwn(result.testData, (count, label) => {
                if (mergedData[label]) {
                    mergedData[label] = 0;
                }
                mergedData[label] += count;
            });
        });

        var failCase = status == Status.Failed
            ? this.failCase ||
              _.find(results, (r) => r.status == Status.Failed).failCase
            : null;

        return new Result(status, passes, discards, mergedData, failCase);
    }
}


export class GroupResult extends Result {
    constructor(
        public status: Status,
        public passes: number,
        public discards: number,
        public testData: any,
        public failCase?: TestCase,
        public results?: {[key: string]: Result}
    ) {
        super(status, passes, discards, testData, failCase, results);
    }
}


// thrown on an implication test failing to prevent running
// the singular case any further than necessary
export class ImplicationFailure {
    public name: string;
    constructor(public message: string) {
        this.name = "ImplicationFailure";
    }
}

export class TestCase {
    private result: boolean;
    private labels: string[];
    private classifications: string[];

    constructor(public args: any[]) {
        this.labels = [];
        this.classifications = [];
    }

    getArg(ix: number): any { return this.args[ix]; }
    getArgs(): any[] { return this.args; }
    getLabels(): string[] { return this.labels; }
    getResult(): boolean { return this.result; }
    setResult(result: boolean) { this.result = result; }
    getClassificationString(): string {
        return this.classifications.join(', ');
    }

    label(labelName: string, showLabel?: boolean): boolean {
        if (!_.isBoolean(showLabel)) {
            this.labels.push(labelName);
        } else if (showLabel) {
            this.labels.push(labelName);
        }

        return _.isBoolean(showLabel) ? showLabel : true;
    }

    implies(implicationResult: boolean) {
        if (!implicationResult) {
            throw new ImplicationFailure("Implication Failed");
        }
        return implicationResult;
    }

    classify(test: boolean, trueLabel: string, falseLabel: string): boolean {
        if (test) {
            this.classifications.push(trueLabel);
        } else if (!test && falseLabel) {
            this.classifications.push(falseLabel);
        }

        return test;
    }

    collect(value: any) {
        this.classifications.push(value.toString());
        return value;
    }
}


export interface RunParameters {
    minTestPasses?: number;
    maxDiscardRatio?: number;
    minSize?: number;
    maxSize?: number;
    explore?: boolean;
    rng?: () => number;
}

export var DefaultRunParameters = {
    minTestPasses: 100,
    maxDiscardRatio: 5,
    minSize: 0,
    maxSize: 100,
    explore: false,
    rng: Math.random,
}

export interface G<T> extends Gen.Generator<T> {}

export interface Property {
    name?: string;
    run(p?: RunParameters): any;
    or(property: Property): CombinedProperty;
    and(property: Property): CombinedProperty;
}


export enum PropertyOperator {
    AND, OR
}

export class BaseProperty implements Property {
    run(p?: RunParameters): any {
        throw new Error("BaseProperty does not implement run method.");
    }

    and(prop: Property): CombinedProperty {
        return new CombinedProperty(PropertyOperator.AND, [this, prop]);
    }

    or(prop: Property): CombinedProperty {
        return new CombinedProperty(PropertyOperator.OR, [this, prop]);
    }

    check(p?: RunParameters): any {
        return PropRunner.check(this, p);
    }

    checkData(p?: RunParameters): any {
        return PropRunner.checkProp(this, p);
    }
}

export class CombinedProperty extends BaseProperty {
    constructor(
        public operator: PropertyOperator,
        public properties: Property[]
    ) {
        super();
    }

    combineResults(results: Result[]): Result {
        switch (this.operator) {
            case PropertyOperator.AND:
            var status = results.reduce((lowest: Status, r: Result) => {
                return r.status < lowest ? r.status : lowest;
            }, Status.Passed);
            return results[0].combine(status, results.slice(1));

            case PropertyOperator.OR:
            var status = results.reduce((highest: Status, r: Result) => {
                return r.status > highest ? r.status : highest;
            }, Status.Failed);
            return results[0].combine(status, results.slice(1));
        };
    }

    run(p?: RunParameters): any {
        var results = this.properties.map((prop) => prop.run(p));
        var promised = results.some((r) => !(r instanceof Result));

        if (!promised) {
            return this.combineResults(results);
        } else {
            var pResults = results.map((r) => {
                return r instanceof Result ? Promise.resolve(r) : r;
            });

            return Promise.all(pResults).then((res) => {
                return this.combineResults(res);
            });
        }
    }
}


export class PropertyGroup extends BaseProperty {
    public properties: {[key: string]: Property};

    constructor(public name: string) {
        super();
        this.properties = {};
    }

    add(propName: string, property: Property) {
        this.properties[propName] = property;
    }

    combineResults(results: {[key: string]: Result}): GroupResult {
        var status = _.values(results).reduce((lowest: Status, r: Result) => {
            return r.status < lowest ? r.status : lowest;
        }, Status.Passed);

        return new GroupResult(
            status,
            _.values(results).filter((r) => r.status == Status.Passed).length,
            _.values(results).filter((r) => r.status == Status.Undecided).length,
            {}, undefined, results
        );
    }

    run(p?: RunParameters): any {
        var results = _.mapValues(this.properties, (prop) => prop.run(p));
        var promised = _.values(results).some((r) => !(r instanceof Result));
        
        if (!promised) {
            return this.combineResults(results);
        } else {
            var keys = _.keys(results);
            var pResults = _.values(results).map((r) => {
                return r instanceof Result ? Promise.resolve(r) : r;
            });

            return Promise.all(pResults).then((res) => {
                return this.combineResults(_.zipOject(keys, res));
            });
        }
    }
}




/// QUANTIFIED PROPERTIES
export class QuantifiedProperty<A, B, C> extends BaseProperty {
    private generators: any[];
    private testFunction: any;

    constructor(g1: G<A>, g2: G<B>, g3: G<B>, func: (a: A, b: B, c: C) => Bluebird.Promise);
    constructor(g1: G<A>, g2: G<B>, func: (a: A, b: B) => Bluebird.Promise);
    constructor(g1: G<A>, func: (a: A) => Bluebird.Promise);
    constructor(g1: G<A>, g2: G<B>, g3: G<B>, func: (a: A, b: B, c: C) => boolean);
    constructor(g1: G<A>, g2: G<B>, func: (a: A, b: B) => boolean);
    constructor(g1: G<A>, func: (a: A) => boolean);
    constructor(
    ) {
        super();
        var args = arguments;

        this.generators = Array.prototype.slice.call(args, 0, args.length - 1);
        this.testFunction = arguments[arguments.length - 1];
    }

    // shrinkTestCase
    //
    // collect all possible shrink candidates from generators, and then
    // aggressively test from smallest to largest
    public shrinkTestCase(testCase: TestCase): TestCase {
        var candidates = this.generators.map((gen, ix) => {
            return gen.shrink(testCase.getArg(ix));
        });

        if (!candidates.every((xs) => xs.length > 0)) {
            // some cases lack candidates, already smallest case
            return testCase;
        };

        var iterator = candidates.map((xs) => 0);

        while (iterator.every((cIx, ix) => cIx < candidates[ix].length - 1)) {
            // advance final iterator of product
            iterator[iterator.length - 1]++;

            // propagate overflow down the list
            for (var ix = iterator.length - 1; ix > -1; ix--) {
                if (iterator[ix] == candidates[ix].length) {
                    iterator[ix] = 0;

                    if (ix - 1 > -1) {
                        // advance next
                        iterator[ix - 1]++;
                    }
                }
            }

            var args = iterator.map((cIx, ix) => candidates[ix][cIx]);
            var nextTestCase = new TestCase(args);

            var nextResult = this.testFunction.apply(nextTestCase, args);
            if (!nextResult) {
                // found failure, return case
                nextTestCase.setResult(nextResult);
                return nextTestCase;
            }
        };

        // all shrunk cases passed
        return testCase;
    }

    public promiseShrinkTestCase(testCase: TestCase): Bluebird.Promise {
        var candidates = this.generators.map((gen, ix) => {
            return gen.shrink(testCase.getArg(ix));
        });

        if (!candidates.every((xs) => xs.length > 0)) {
            // some cases lack candidates, already smallest case
            return Promise.resolve(testCase);
        };

        var iterator = candidates.map((xs) => 0);
        var results = [];

        while (iterator.every((cIx, ix) => cIx < candidates[ix].length - 1)) {
            // advance final iterator of product
            iterator[iterator.length - 1]++;

            // propagate overflow down the list
            for (var ix = iterator.length - 1; ix > -1; ix--) {
                if (iterator[ix] == candidates[ix].length) {
                    iterator[ix] = 0;

                    if (ix - 1 > -1) {
                        // advance next
                        iterator[ix - 1]++;
                    }
                }
            }

            var args = iterator.map((cIx, ix) => candidates[ix][cIx]);
            var nextTestCase = new TestCase(args);

            try {
                var result = this.testFunction.apply(nextTestCase, args);
            } catch (e) {
                if (e.name == "ImplicationFailure") {
                    continue;
                } else {
                    throw e;
                }
            }

            if (typeof result === 'boolean') {
                nextTestCase.setResult(result);
                results.push(Promise.resolve(nextTestCase));
            } else {
                (function() {
                var promisedCase = nextTestCase;
                results.push(result.then((res) => {
                    promisedCase.setResult(res);
                    return promisedCase;
                }));

                })();
            }
        };

        // all shrunk cases passed
        return Promise.settle(results).then((resultSet) => {
            for (var ix = 0; ix < resultSet.length; ix++) {
                var pi = resultSet[ix];
                
                if (pi.isRejected()) {
                    if (pi._settledValue.name == 'ImplicationFailure') {
                        continue;
                    } else {
                        throw pi._settledValue;
                    }
                } else {
                    var nextTestCase = pi.value();
                }

                if (nextTestCase.getResult()) {
                    continue;
                } else {
                    return nextTestCase; // failed case
                }
            };

            return testCase;
        });

    }

    run(p?: RunParameters): any {
        p = p || {};
        _.defaults(p, DefaultRunParameters);
        
        var stepSize = (p.maxSize-p.minSize) / p.minTestPasses;

        var results: TestCase[] = [];
        var passes = 0;
        var discards = 0;
        var currentSize = 0;
        var collectedTestData = {};

        var discardRatioPassed = function() {
            return (discards + passes > p.minTestPasses &&
                    discards / p.maxDiscardRatio >= p.minTestPasses);
        }

        while (passes < p.minTestPasses) {
            if (discardRatioPassed()) { break; }

            var args = this.generators.map(
                (gen) => gen.arbitrary(p.rng, currentSize)
            );

            var testCase = new TestCase(args);

            try {
                var result = this.testFunction.apply(testCase, args);
            } catch (e) {
                if (e.name == "ImplicationFailure") {
                    discards++;
                    currentSize += stepSize;
                    continue;
                } else {
                    throw e;
                }
            }

            if (typeof result !== 'boolean' && result['then']) {
                if (result.isRejected()) {
                    if (result._settledValue.name != 'ImplicationFailure') {
                        return Promise.reject(result._settledValue);
                    }
                }

                return this.finishRunPromised(
                    results, p, currentSize, stepSize
                );
            } else if (typeof result !== 'boolean') {
                throw new Error("Property not returning boolean or promise.");
            }

            testCase.setResult(result);
            results.push(testCase);

            if (!testCase.getResult()) {
                break;
            } else {
                var classString = testCase.getClassificationString();
                if (classString) {
                    if (_.isUndefined(collectedTestData[classString])) {
                        collectedTestData[classString] = 0
                    }
                    collectedTestData[classString]++;
                }

                currentSize += stepSize;
                passes++;
            }
        }

        // return result of testing
        if (discardRatioPassed()) {
            return new Result(
                Status.Undecided,
                passes, discards,
                collectedTestData
            );
        } else if (passes < p.minTestPasses) {
            var failCase = _.find(results, (res) => !res.result);
            var shrunkFailCase = this.shrinkTestCase(failCase);

            return new Result(
                Status.Failed,
                passes, discards,
                collectedTestData,
                shrunkFailCase
            );
        } else {
            return new Result(
                Status.Passed,
                passes, discards,
                collectedTestData
            );
        }
    }


    finishRunPromised(
        tresults: TestCase[], p: RunParameters, size: number, step: number
    ): Bluebird.Promise {
        var results = tresults.map(Promise.resolve);

        var sprintProgress = results.length;
        var sprintEnd = p.minTestPasses;
        var stepSize = step;
        var currentSize = size;
        var discards = 0;

        var discardRatioPassed = function() {
            return (discards + sprintProgress > p.minTestPasses &&
                    discards / p.maxDiscardRatio >= p.minTestPasses);
        }

        var doSprint = () => {
            while (sprintProgress < sprintEnd) {
                if (discardRatioPassed()) { break; }

                var args = this.generators.map(
                    (gen) => gen.arbitrary(p.rng, currentSize)
                );

                var testCase = new TestCase(args);

                try {
                    var result = this.testFunction.apply(testCase, args);
                } catch (e) {
                    if (e.name == "ImplicationFailure") {
                        discards++;
                        currentSize += stepSize;
                        continue;
                    } else {
                        throw e;
                    }
                }

                if (typeof result === 'boolean') {
                    testCase.setResult(result);
                    results.push(Promise.resolve(testCase));
                } else {
                    (function() {
                    var promisedCase = testCase;
                    results.push(result.then((res) => {
                        promisedCase.setResult(res);
                        return promisedCase;
                    }));

                    })();
                }
                currentSize += stepSize;
                sprintProgress++;
            }

            return Promise.settle(results).then((resultSet) => {
                var collectedTestData = {}
                var passes = 0;
                var discardRatioPassed = function() {
                    return (discards + passes > p.minTestPasses &&
                            discards / p.maxDiscardRatio >= p.minTestPasses);
                }

                for (var ix = 0; ix < resultSet.length; ix++) {
                    var pi = resultSet[ix];
                    
                    if (pi.isRejected()) {
                        if (pi._settledValue.name == 'ImplicationFailure') {
                            discards++;
                            continue;
                        } else {
                            throw pi._settledValue;
                        }
                    } else {
                        var testCase = pi.value();
                    }

                    if (!testCase.getResult()) {
                        break;
                    } else {
                        var classString = testCase.getClassificationString();
                        if (classString) {
                            if (_.isUndefined(collectedTestData[classString])) {
                                collectedTestData[classString] = 0
                            }
                            collectedTestData[classString]++;
                        }

                        passes++;
                    }
                };

                var failCase = _.find(resultSet, (res) => {
                    return res.isRejected() ? false : !res.value().result;
                });
                // return result of testing
                if (discardRatioPassed()) {
                    return new Result(
                        Status.Undecided,
                        passes, discards,
                        collectedTestData
                    );
                } else if (failCase) {
                    // promised not shrinking for now
                    // var shrunkFailCase = this.shrinkTestCase(failCase);
                    var pR = this.promiseShrinkTestCase(failCase.value())
                    return pR.then((shrunk) => {
                        return new Result(
                            Status.Failed,
                            passes, discards,
                            collectedTestData,
                            shrunk
                        );
                    });

                } else if (passes >= p.minTestPasses) {
                    return new Result(
                        Status.Passed,
                        passes, discards,
                        collectedTestData
                    );
                } else {
                    sprintEnd += p.minTestPasses;
                    return doSprint();
                }
            });
        };
        return doSprint();
    }
}



export class QuantifiedPropertyNoShrink<A, B, C>
       extends QuantifiedProperty<A,B,C> {
    public shrinkTestCase(testCase: TestCase): TestCase {
        return testCase;
    }
}






// SHORTHAND INTERFACE

// late import to prevent circular problems
import Commands = require('./Commands');


export function forAll<A, B, C>(
    g1: G<A>, g2: G<B>, g3: G<C>, func: (a: A, b: B, c: C) => Bluebird.Promise
): QuantifiedProperty<A, B, C>;
export function forAll<A, B, C>(
    g1: G<A>, g2: G<B>, func: (a: A, b: B) => Bluebird.Promise
): QuantifiedProperty<A, B, C>;
export function forAll<A, B, C>(
    g1: G<A>, func: (a: A) => Bluebird.Promise
): QuantifiedProperty<A, B, C>;
export function forAll<A, B, C>(
    g1: G<A>, g2: G<B>, g3: G<C>, func: (a: A, b: B, c: C) => boolean
): QuantifiedProperty<A, B, C>;
export function forAll<A, B, C>(
    g1: G<A>, g2: G<B>, func: (a: A, b: B) => boolean
): QuantifiedProperty<A, B, C>;
export function forAll<A, B, C>(
    g1: G<A>, func: (a: A) => boolean
): QuantifiedProperty<A, B, C>;
export function forAll<A, B, C>(...args: any[]): QuantifiedProperty<A, B, C> {
    var P = function(argv) { QuantifiedProperty.apply(this, argv) };
    P.prototype = QuantifiedProperty.prototype;
    return new P(args);
}


export function forAllNoShrink<A, B, C>(
    g1: G<A>, g2: G<B>, g3: G<C>, func: (a: A, b: B, c: C) => Bluebird.Promise
): QuantifiedPropertyNoShrink<A, B, C>;
export function forAllNoShrink<A, B, C>(
    g1: G<A>, g2: G<B>, func: (a: A, b: B) => Bluebird.Promise
): QuantifiedPropertyNoShrink<A, B, C>;
export function forAllNoShrink<A, B, C>(
    g1: G<A>, func: (a: A) => Bluebird.Promise
): QuantifiedPropertyNoShrink<A, B, C>;
export function forAllNoShrink<A, B, C>(
    g1: G<A>, g2: G<B>, g3: G<C>, func: (a: A, b: B, c: C) => boolean
): QuantifiedPropertyNoShrink<A, B, C>;
export function forAllNoShrink<A, B, C>(
    g1: G<A>, g2: G<B>, func: (a: A, b: B) => boolean
): QuantifiedPropertyNoShrink<A, B, C>;
export function forAllNoShrink<A, B, C>(
    g1: G<A>, func: (a: A) => boolean
): QuantifiedPropertyNoShrink<A, B, C>;
export function forAllNoShrink<A, B, C>(...args: any[]): QuantifiedPropertyNoShrink<A, B, C> {
    var P = function(argv) { QuantifiedPropertyNoShrink.apply(this, argv) };
    P.prototype = QuantifiedPropertyNoShrink.prototype;
    return new P(args);
}


export function all(...properties: Property[]): CombinedProperty {
    return new CombinedProperty(PropertyOperator.AND, properties);
}

export function atLeastOne(...properties: Property[]): CombinedProperty {
    return new CombinedProperty(PropertyOperator.OR, properties);
}

export function group(name: string): PropertyGroup {
    return new PropertyGroup(name);
}

export function commands(commandSpec): Commands.CommandsProperty {
    return new Commands.CommandsProperty(commandSpec);
}
