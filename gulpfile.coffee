gulp = require 'gulp'
gutil = require 'gulp-util'
using = require 'gulp-using'
jade = require 'gulp-jade'
debug = require 'gulp-debug'
concat = require 'gulp-concat'
connect = require 'gulp-connect'
wrap = require 'gulp-wrap-amd'
rename = require 'gulp-rename'
through = require 'through2'
tsc = require 'gulp-tsc'
browserify = require 'gulp-browserify'
shell = require 'gulp-shell'
watch = require 'gulp-watch'


source =
  ts: 'src'
  tests: 'tests'
  docs: 'docs'

target =
  js: '.tmp/src'
  root: '.tmp'
  tests: '.tmp/tests'
  build: 'build'
  docs: 'docs/_build/html'

gulp.task "tsc", ->
  gulp.src("{src,tests}/**/*.ts")
    .pipe(tsc(outDir: "#{ target.root }", emitError: false, target: "ES5"))
    .pipe(gulp.dest("#{ target.root }"))
    .pipe(using(prefix: 'Output JS to'))

gulp.task "build:node", ->
  gulp.src("src/**/*.ts")
    .pipe(tsc(outDir: "#{ target.build }", emitError: false, target: "ES5"))
    .pipe(gulp.dest("#{ target.build }/node"))
    .pipe(using(prefix: 'Output Built JS to'))

gulp.task 'browserify:src', ->
  gulp.src("#{ target.js }/SwiftCheck.js")
    .pipe(browserify())
    .pipe(rename('SwiftCheck.js'))
    .pipe(gulp.dest(target.build))
    .pipe(using(prefix: 'Output Bundle to'))

gulp.task 'browserify:tests', ->
  gulp.src("#{ target.tests }/UnitTestBase.js")
    .pipe(browserify())
    .pipe(rename('tests.js'))
    .pipe(gulp.dest(target.build))
    .pipe(using(prefix: 'Output Bundle to'))


gulp.task "test", ['tsc'], ->
  gulp.src("#{ target.tests }/UnitTestBase.js")
    .pipe(shell([
      "./node_modules/mocha/bin/mocha <%= file.path %>"
    ]))

gulp.task "build", ['tsc', 'build:node'], ->
  gulp.start("browserify:tests")
  gulp.start("browserify:src")

gulp.task 'default', ['tsc'],  ->
  watch glob: "#{ source.ts }/**/*.ts", (files) ->
    files.pipe(tsc(outDir: "#{ target.js }", emitError: false, target: "ES5"))
         .pipe(gulp.dest("#{ target.js }"))
         .pipe(using(prefix: 'Output JS to'))

  watch glob: "#{ source.tests }/**/*.ts", (files) ->
    files.pipe(tsc(
      outDir: "#{ target.root }", sourceRoot: source.root, emitError: false,
      target: "ES5"
    ))
         .pipe(gulp.dest("#{ target.root }"))
         .pipe(using(prefix: 'Output JS to'))

  gulp.watch("#{ target.js }/**/*.js", ["browserify:src"])
  gulp.watch("#{ target.root }/**/*.js", ["browserify:tests"])
