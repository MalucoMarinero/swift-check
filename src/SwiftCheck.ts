///<reference path="../typedefs/node.d.ts"/>

import Properties = require('./Properties');
import Generators = require('./Generators');
import PropRunner = require('./PropRunner');

module SwiftCheck {
    export var Prop = Properties;
    export var Gen = Generators;
    export var Runner = PropRunner;
}


export = SwiftCheck;
