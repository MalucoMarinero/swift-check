///<reference path="../typedefs/node.d.ts"/>
var Properties = require('./Properties');
var Generators = require('./Generators');
var PropRunner = require('./PropRunner');

var SwiftCheck;
(function (SwiftCheck) {
    SwiftCheck.Prop = Properties;
    SwiftCheck.Gen = Generators;
    SwiftCheck.Runner = PropRunner;
})(SwiftCheck || (SwiftCheck = {}));

module.exports = SwiftCheck;
