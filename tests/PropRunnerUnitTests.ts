///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/sinon.d.ts"/>
///<reference path="../typedefs/chai.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>

import PropRunner = require("../src/PropRunner");
declare var describe, it, before, after, afterEach, beforeEach; // Mocha injections.
var expect = require("chai").expect;
var sinon = require("sinon");
var _ = require('lodash');
var Promise: Bluebird.PromiseStatic = require('bluebird');

var PropStatus = {};
PropStatus['Failed'] = 0;
PropStatus['Undecided'] = 1;
PropStatus['Passed'] = 2;

var mockSuccess = {
    status: PropStatus['Passed'],
    passes: 100,
    discards: 0,
    testData: {},
    failCase: null
}

var mockProperty = {
    run: function(parameters?) { return mockSuccess; },
    or: function(val) { return mockProperty; },
    and: function(val) { return mockProperty; },
    check: function(p): any { return {}},
    checkData: function(p): any { return {}},
}

var mockGroupSuccess = {
    status: PropStatus['Passed'],
    results: {
        "pass1": mockSuccess,
        "pass2": mockSuccess
    },
    passes: 2,
    discards: 0,
    testData: {},
    failCase: null,
};

var mockGroupProperty = {
    name: "MockGroup",
    run: function(parameters?) { return mockGroupSuccess; },
    or: function(val) { return mockProperty; },
    and: function(val) { return mockProperty; },
    properties: {
        "pass1": mockProperty,
        "pass2": mockProperty,
    },
    add: function(str: string) {},
    combineResults: function(val) { return mockGroupSuccess; },
}

var consoleStub;
before(() => {
    consoleStub = sinon.stub(console, "log");
});

beforeEach(() => {
    if (mockProperty.run['restore']) {
        mockProperty.run['restore']()
    }
    consoleStub.reset();
});


after(() => {
    consoleStub.restore();
});

describe('PropRunner To Property Collaboration', () => {
    describe('checkProp#', () => {
    it('should call run() with no parameters if none given', () => {
        var spy = sinon.spy(mockProperty, "run");

        PropRunner.checkProp(mockProperty);
        expect(spy.calledWith(undefined)).to.be.true;
        spy.restore();
    });

    it('should call run() with parameters if given', () => {
        var spy = sinon.spy(mockProperty, "run");

        PropRunner.checkProp(mockProperty, {minTestPasses: 200});
        expect(spy.calledWith({minTestPasses: 200})).to.be.true;
        spy.restore();
    });
    });
});


describe('PropRunner Log Output', () => {
    describe('checkProp', () => {
    it('should output test passes message if result passes', () => {
        PropRunner.checkProp(mockProperty);
        expect(consoleStub.called).to.be.true;
        expect(consoleStub.args[0][0]).to.contain(
            "OK, passed 100 tests."
        );
    });

    // COMMENTED OUT BECAUSE THE TYPE SYSTEM EXPLODES WITH THIS MOCK
    // it('should output test passes for all sub props in property groups', () => {
    //     PropRunner['checkProp'](mockGroupProperty);
    //     expect(consoleStub.called).to.be.true;
    //     expect(consoleStub.args[0][0]).to.contain("MockGroup.pass1");
    //     expect(consoleStub.args[1][0]).to.contain("MockGroup.pass2");
    // });

    it('should handle Promises Correctly', (done) => {
        var propStub = sinon.stub(mockProperty, "run").returns(
            Promise.resolve({
                status: PropStatus['Passed'],
                passes: 100, discards: 0, testData: {}, failCase: null
            })
        );

        PropRunner.checkProp(mockProperty).then((res) => {
            expect(consoleStub.called).to.be.true;
            expect(consoleStub.args[0][0]).to.contain(
                "OK, passed 100 tests."
            );
            propStub.restore();
            done();
        }).catch(done);

    });


    it('should throw PropertyUndecided if result undecided', () => {
        var propStub = sinon.stub(mockProperty, "run").returns({
            status: PropStatus['Undecided'],
            passes: 20, discards: 500, testData: {}, failCase: null
        });

        var message;
        try {
            PropRunner.checkProp(mockProperty);
        } catch (e) {
            message = e.message;
        }
        expect(message).to.contain("Gave up after only 20 passed tests.");
        expect(message).to.contain("500 tests were discarded.");
        propStub.restore();
    });

    // COMMENTED OUT BECAUSE THE TYPE SYSTEM EXPLODES WITH THIS MOCK
    // it('should throw PropertyFailure if one of a prop group failed', () => {
    //     var propStub = sinon.stub(mockGroupProperty, "run").returns({
    //         status: PropStatus['Failed'],
    //         results: {
    //             "pass1": mockSuccess,
    //             "fail2": {
    //                 status: PropStatus['Failed'],
    //                 passes: 20, discards: 0, testData: {},
    //                 failCase: {
    //                     getArgs: () => [25],
    //                     getLabels: () => []
    //                 }
    //             }
    //         },
    //         passes: 1,
    //         discards: 0,
    //         testData: {},
    //         failCase: null,
    //     });

    //     var message;
    //     try {
    //         PropRunner.checkProp(mockGroupProperty);
    //     } catch (e) {
    //         message = e.message;
    //     }
    //     expect(message).to.contain("MockGroup.fail2");
    //     expect(message).to.contain("Failed after 20 tests.");
    //     expect(message).to.contain("> ARG 0: 25");
    //     propStub.restore();
    // });

    it('should throw PropertyFailure if result failed', () => {
        var propStub = sinon.stub(mockProperty, "run").returns({
            status: PropStatus['Failed'],
            passes: 20, discards: 0, testData: {},
            failCase: {
                getArgs: () => [25],
                getLabels: () => []
            }
        });

        var message;
        try {
            PropRunner.checkProp(mockProperty);
        } catch (e) {
            message = e.message;
        }
        expect(message).to.contain("Failed after 20 tests.");
        expect(message).to.contain("> ARG 0: 25");
        propStub.restore();
    });

    it('should output labels if result failed', () => {
        var propStub = sinon.stub(mockProperty, "run").returns({
            status: PropStatus['Failed'],
            passes: 20, discards: 0, testData: {},
            failCase: {
                getArgs: () => [25],
                getLabels: () => ["this label was triggered"]
            }
        });

        var message;
        try {
            PropRunner.checkProp(mockProperty);
        } catch (e) {
            message = e.message;
        }
        expect(message).to.contain("this label was triggered");
        propStub.restore();
    });

    it('should output test data if test passes', () => {
        var propStub = sinon.stub(mockProperty, "run").returns({
            status: PropStatus['Passed'],
            passes: 100, discards: 0, testData: {
                "one": 20, "two": 10, "three": 25, "four": 25, "five": 20,
            }
        });

        PropRunner.checkProp(mockProperty);

        expect(consoleStub.called).to.be.true;
        expect(consoleStub.args[0][0]).to.contain("20% one");
        expect(consoleStub.args[0][0]).to.contain("10% two");
        expect(consoleStub.args[0][0]).to.contain("25% three");
        expect(consoleStub.args[0][0]).to.contain("25% four");
        expect(consoleStub.args[0][0]).to.contain("20% five");

        propStub.restore();
    });
    });

    describe('check', () => {
    it('should not output to the console on success', () => {
        PropRunner.check(mockProperty);
        expect(consoleStub.called).to.be.false;
    });

    it('should throw PropertyUndecided if result undecided', () => {
        var propStub = sinon.stub(mockProperty, "run").returns({
            status: PropStatus['Undecided'],
            passes: 20, discards: 500, testData: {}, failCase: null
        });

        var message;
        try {
            PropRunner.check(mockProperty);
        } catch (e) {
            message = e.message;
        }
        expect(message).to.contain("Gave up after only 20 passed tests.");
        expect(message).to.contain("500 tests were discarded.");
        propStub.restore();
    });

    it('should throw PropertyFailure if result failed', () => {
        var propStub = sinon.stub(mockProperty, "run").returns({
            status: PropStatus['Failed'],
            passes: 20, discards: 0, testData: {},
            failCase: {
                getArgs: () => [25],
                getLabels: () => []
            }
        });

        var message;
        try {
            PropRunner.check(mockProperty);
        } catch (e) {
            message = e.message;
        }
        expect(message).to.contain("Failed after 20 tests.");
        expect(message).to.contain("> ARG 0: 25");
        propStub.restore();
    });

    it('should output labels if result failed', () => {
        var propStub = sinon.stub(mockProperty, "run").returns({
            status: PropStatus['Failed'],
            passes: 20, discards: 0, testData: {},
            failCase: {
                getArgs: () => [25],
                getLabels: () => ["this label was triggered"]
            }
        });

        var message;
        try {
            PropRunner.check(mockProperty);
        } catch (e) {
            message = e.message;
        }
        expect(message).to.contain("this label was triggered");
        propStub.restore();
    });

    });
});
