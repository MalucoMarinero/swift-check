///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Promise = require('bluebird');
var _ = require('lodash');
var colors = require('colors');

var Prop = require('./Properties');

var PropertyFailure = (function (_super) {
    __extends(PropertyFailure, _super);
    function PropertyFailure(result, name) {
        this.failCase = result.failCase;
        this.result = result;

        this.name = "PropertyFailure";
        this.message = "";
        if (name) {
            this.message += name + ' ';
        }
        this.message += "PropertyFailure: Failed after " + result.passes;
        this.message += " tests.\n";

        if (this.failCase.getLabels().length > 0) {
            this.message += this.failCase.getLabels().map(function (label) {
                return ":: " + label;
            }).join("\n") + "\n";
        }
        ;

        this.message += this.failCase.getArgs().map(function (arg, ix) {
            return "> ARG " + ix + ": " + arg.toString();
        }).join("\n");
    }
    return PropertyFailure;
})(Error);
exports.PropertyFailure = PropertyFailure;

var PropertyUndecided = (function (_super) {
    __extends(PropertyUndecided, _super);
    function PropertyUndecided(result, name) {
        this.name = "PropertyUndecided";
        this.result = result;
        this.message = "";
        if (name) {
            this.message += name + ' ';
        }
        this.message += "PropertyUndecided: Gave up after only ";
        this.message += result.passes + " passed tests. ";
        this.message += result.discards + " tests were discarded.";
    }
    return PropertyUndecided;
})(Error);
exports.PropertyUndecided = PropertyUndecided;

function reportSuccess(result, name) {
    if (name) {
        var output = "\n+ " + name + " OK, passed ";
        output += result.passes + " tests.\n";
    } else {
        var output = "\n+ OK, passed " + result.passes + " tests.\n";
    }

    var collectedOutput = "";

    _.forOwn(result.testData, function (num, name) {
        var perc = Math.floor(num / result.passes * 100);
        collectedOutput += perc + "% " + name + "\n";
    });

    if (collectedOutput) {
        output += "> Collected test data:\n";
        output += collectedOutput;
    }

    console.log(output["green"]);
}

function processResult(result, name, silent) {
    if (result.results) {
        _.forOwn(result.results, function (res, key) {
            var propName = name + '.' + key;
            switch (res.status) {
                case 2 /* Passed */:
                    if (!silent) {
                        reportSuccess(res, propName);
                    }
                    break;

                case 1 /* Undecided */:
                    throw new PropertyUndecided(res, propName);
                    break;

                case 0 /* Failed */:
                    throw new PropertyFailure(res, propName);
                    break;
            }
            ;
        });
    } else {
        switch (result.status) {
            case 2 /* Passed */:
                if (!silent) {
                    reportSuccess(result, name);
                }
                return result;

            case 1 /* Undecided */:
                throw new PropertyUndecided(result, name);
                break;

            case 0 /* Failed */:
                throw new PropertyFailure(result, name);
                break;
        }
    }

    return result;
}

function checkProp(prop, parameters) {
    var result = prop.run(parameters);

    if (result['then']) {
        return result.then(function (res) {
            return processResult(res, prop.name);
        });
    } else {
        return processResult(result, prop.name);
    }
}
exports.checkProp = checkProp;

function check(prop, parameters) {
    var result = prop.run(parameters);

    if (result['then']) {
        return result.then(function (res) {
            return processResult(res, prop.name, true);
        });
    } else {
        return processResult(result, prop.name, true);
    }
}
exports.check = check;
