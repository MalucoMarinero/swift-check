///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>

var _ = require('lodash');

export interface Generator<T> {
    arbitrary(rng: () => number, size: number): T;
    shrink(variable: T): T[];
}


export class BaseGenerator<T> implements Generator<T> {
    arbitrary(rng, size): T {
        throw new Error("BaseGenerator doesn't implement arbitrary");
    }

    shrink(variable: T): T[] {
        throw new Error("BaseGenerator doesn't implement shrink");
    }

    suchThat(func: (value: T) => boolean): FilteredGenerator<T> {
        return new FilteredGenerator<T>(this, func);
    }
}


export class FilteredGenerator<T> extends BaseGenerator<T> {
    constructor(
        public wrapped: Generator<T>,
        public filter: (value: T) => boolean
    ) {
        super();
    }

    arbitrary(rng, size): T {
        var tryCounter = 0;
        var genValue = this.wrapped.arbitrary(rng, size);

        while (!this.filter(genValue)) {
            tryCounter++;
            if (tryCounter > 100) {
                throw new Error("Generator suchThat filter too restrictive");
            }

            genValue = this.wrapped.arbitrary(rng, size);
        }

        return genValue;
    }

    shrink(value: T): T[] {
        var shrinks = this.wrapped.shrink(value);
        shrinks = shrinks.filter(this.filter);
        return shrinks.length == 0 ? [value] : shrinks;
    }
}

export function map<T,A>(
    gen: Generator<T>, func: (value: T) => A
): MapGenerator<T,A> {
    return new MapGenerator<T,A>(gen, func);
}

export class MapGenerator<T,A> extends BaseGenerator<T> {
    constructor(
        public wrapped: Generator<T>,
        public map: (value: T) => A
    ) {
        super();
    }

    arbitrary(rng, size): any {
        return this.map(this.wrapped.arbitrary(rng, size));
    }

    shrink(value: T): any[] {
        return this.wrapped.shrink(value).map(this.map);
    }
}


export function oneOf<T>(vals: Array<T>): OneOfGenerator<T>;
export function oneOf<T>(...values: Array<T>): OneOfGenerator<T>;
export function oneOf<T>(): OneOfGenerator<T> {
    if (_.isArray(arguments[0])) {
        return new OneOfGenerator<T>(arguments[0]);
    } else {
        return new OneOfGenerator<T>(Array.prototype.slice.call(arguments, 0));
    };
};

export function frequency<T>(...values: Array<any>): OneOfGenerator<T>;
export function frequency<T>(): FrequencyGenerator<T> {
    if (_.isArray(arguments[0])) {
        return new FrequencyGenerator<T>(arguments[0]);
    } else {
        return new FrequencyGenerator<T>(Array.prototype.slice.call(arguments, 0));
    };
};


interface FrequencyAllotment {
    min: number;
    max: number;
    ix: number;
}

export class FrequencyGenerator<T> extends BaseGenerator<T> {
    public frequencies: number[];
    public values: T[];
    private allotment: FrequencyAllotment[];
    public frequencyTotal: number;

    constructor(values: any[]) {
        super();
        this.frequencies = values.map((v) => v[0]);
        this.allotment = [];

        this.frequencyTotal = this.frequencies.reduce((acc, f1, ix) => {
            var max = acc + f1;
            this.allotment.push({
                min: acc, max: max, ix: ix
            });

            return max;
        }, 0);
        this.allotment[this.allotment.length -1].max++;
        this.values = values.map((v) => v[1]);
    }

    arbitrary(rng, size): T {
        var target = Math.round(rng() * this.frequencyTotal);
        var ix = _.find(this.allotment, (a) => {
            return (a.min <= target) && (a.max > target);
        }).ix;
        return this.values[ix];
    }

    shrink(val: T): T[] {
        return [val];
    }
}

export class OneOfGenerator<T> extends FrequencyGenerator<T> {
    constructor(values: T[]) {
        super(values.map((v) => [1, v]));
    }
}



export function arrayOf<T>(
    gen: Generator<T>, minSize?: number, maxSize?: number
): Generator<Array<T>> {
    return new ArrayGenerator<T>(gen, minSize, maxSize);
}


export class ArrayGenerator<T> extends BaseGenerator<Array<T>> {
    public wrapped: Generator<T>;

    constructor(
        gen: Generator<T>, public minSize?: number, public maxSize?: number
    ) {
        super();
        this.wrapped = gen;
        if (minSize < 0) {
            throw new Error("Arrays cannot be smaller than 0 length.");
        }

        if (minSize && maxSize && maxSize < minSize) {
            throw new Error("Maximum Size must be greater than minimum size.");
        }
        if (!minSize) { this.minSize = 0; }
    }

    arbitrary(rng, size): Array<T> {
        var upper = this.maxSize ? this.maxSize : size;
        var sizeMultiplier = size == 0 ? size : size / 100;
        var range = upper - this.minSize;
        var multi = rng() * sizeMultiplier;
        var arrayLength = this.minSize + ((rng() * sizeMultiplier)* range);

        return _.range(arrayLength).map((ix) => {
            return this.wrapped.arbitrary(rng, size);
        });
    }

    shrink(value: Array<T>): Array<Array<T>> {
        var shrinkCollections = _.range(value.length + 1).map((len) => {
            var valShrinks = _.range(len).map((ix) => {
                return _.shuffle(this.wrapped.shrink(value[ix]));
            });

            var arrayZips = _.zip.apply(null, valShrinks);

            // remove empty cells
            return arrayZips.map((xs) => xs.filter((x) => !_.isUndefined(x)));
        });

        return _.flatten(shrinkCollections, true);
    }
}




export function sized<T>(
    func: (rng: () => number, size: number) => T
): SizedGenerator<T> {
    return new SizedGenerator<T>(func);
}


export class SizedGenerator<T> extends BaseGenerator<T> {
    public arbitraryFunc: (rng: () => number, size: number) => T;
    public lastSize: number;
    public lastRng: () => number;

    constructor(
        func: (rng: () => number, size: number) => T
    ) {
        super();
        this.arbitraryFunc = func;
    }

    arbitrary(rng, size): T {
        this.lastRng = rng;
        this.lastSize = size;
        return this.arbitraryFunc(rng, size);
    }

    shrink(value: T): T[] {
        var shrinks = [];
        while (shrinks.length < 30) {
            try {
                shrinks.push(this.arbitrary(
                    this.lastRng, this.lastSize - 1
                ));
            } catch (e) {
                break;
            }
        }

        return (shrinks.length < 1) ? [value] : shrinks;
    }
}

function isInteger(n: number): boolean {
    return n === +n && n === (n|0);
};

export function chooseInt(min: number, max: number): ChooseGenerator<number> {
    if (!isInteger(min) || !isInteger(max)) {
        throw new Error("ChooseInt expects integers as range values.");
    }

    return new ChooseGenerator<number>(min, max, "integer");
}

export function choose<T>(min: T, max: T): ChooseGenerator<T> {
    return new ChooseGenerator<T>(min, max);
}

export class ChooseGenerator<T> extends BaseGenerator<T> {
    public type: string;

    constructor(public min: any, public max: any, public modifier?: string) {
        super();
        if (typeof min != typeof max) {
            throw new Error("Cannot generate range between unlike types");
        }
        if (min > max) {
            throw new Error("Cannot generate range, min greater than max");
        }
        this.type = typeof min;

        if (this.type == "string" && (min.length > 1 || max.length > 1)) {
            throw new Error("Cannot create ranges between strings, only chars");
        }

        if (this.type == "string") {
            this.min = this.min.charCodeAt();
            this.max = this.max.charCodeAt();
        }
    }

    arbitrary(rng, size): any {
        switch (this.type) {
            case "number":
            if (this.modifier && this.modifier == "integer") {
                return this.chooseInteger(rng, size);
            } else {
                return this.chooseNumber(rng, size);
            }

            case "string":
            return this.chooseCharacter(rng, size);
        }
    }

    shrink(value): any[] {
        switch (this.type) {
            case "number":
            if (this.modifier && this.modifier == "integer") {
                return this.shrinkInteger(value);
            } else {
                return this.shrinkNumber(value);
            }

            case "string":
            return this.shrinkCharacter(value);
        }
    }

    private chooseNumber(rng, size): number {
        var sizeMultiplier = size == 0 ? size : size / 100;
        var range = this.max - this.min;
        var multi = rng() * sizeMultiplier;
        var num = this.min + ((rng() * sizeMultiplier)* range);

        return num > this.max ? this.max : num;
    }

    private chooseInteger(rng, size): number {
        return Math.round(this.chooseNumber(rng, size));
    }

    private chooseCharacter(rng, size): string {
        var charCode = this.chooseInteger(rng, size);
        return String.fromCharCode(charCode);
    }

    private shrinkNumber(value): number[] {
        var range = value - this.min;

        return _.range(30).map((factor) => {
            return this.min + (range * (factor / 30));
        });
    }

    private shrinkInteger(value): number[] {
        // get result of shrinkNumber, round all and remove duplicates
        return _.uniq(
            this.shrinkNumber(value).map((val) => Math.round(val))
        );
    }

    private shrinkCharacter(value): string[] {
        var charCodes = this.shrinkInteger(value.charCodeAt());
        return charCodes.map((c) => String.fromCharCode(c));
    }
}


export module Values {
    export function string() {
        return map(arrayOf(choose(" ", "~")), (val) => val.join(""));
    }

    export function boolean() { return oneOf(true, false); }

    export function date(min?: Date, max?: Date) {
        if (!min) min = new Date(0);
        if (!max) max = new Date(+new Date() + 100000000);

        return map(chooseInt((+min|0), (+max|0)), (val) => new Date(val));
    }

    var zero = 0;
    var bigNeg = -9007199254740992;
    var bigPos = 9007199254740992;
    var pos = 2147483647;
    var neg = -2147483648;
    var smallPos = 32767;
    var smallNeg = -32768;
    var tinyPos = 255;
    var tinyNeg = -255;

    export function bigInt() { return chooseInt(bigNeg, bigPos); }
    export function bigPositiveInt() { return chooseInt(zero, bigPos); }
    export function bigNegativeInt() { return chooseInt(bigNeg, zero); }
    export function smallInt() { return chooseInt(smallNeg, smallPos); }
    export function smallPositiveInt() { return chooseInt(zero, smallPos); }
    export function smallNegativeInt() { return chooseInt(smallNeg, zero); }
    export function int() { return chooseInt(neg, pos); }
    export function positiveInt() { return chooseInt(zero, pos); }
    export function negativeInt() { return chooseInt(neg, zero); }
    export function tinyInt() { return chooseInt(tinyNeg, tinyPos); }
    export function tinyPositiveInt() { return chooseInt(zero, tinyPos); }
    export function tinyNegativeInt() { return chooseInt(tinyNeg, zero); }

    export function bigFloat() { return choose(bigNeg, bigPos); }
    export function bigPositiveFloat() { return choose(zero, bigPos); }
    export function bigNegativeFloat() { return choose(bigNeg, zero); }
    export function smallFloat() { return choose(smallNeg, smallPos); }
    export function smallPositiveFloat() { return choose(zero, smallPos); }
    export function smallNegativeFloat() { return choose(smallNeg, zero); }
    export function float() { return choose(neg, pos); }
    export function positiveFloat() { return choose(zero, pos); }
    export function negativeFloat() { return choose(neg, zero); }
    export function tinyFloat() { return choose(tinyNeg, tinyPos); }
    export function tinyPositiveFloat() { return choose(zero, tinyPos); }
    export function tinyNegativeFloat() { return choose(tinyNeg, zero); }
}
