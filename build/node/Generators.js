///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var _ = require('lodash');

var BaseGenerator = (function () {
    function BaseGenerator() {
    }
    BaseGenerator.prototype.arbitrary = function (rng, size) {
        throw new Error("BaseGenerator doesn't implement arbitrary");
    };

    BaseGenerator.prototype.shrink = function (variable) {
        throw new Error("BaseGenerator doesn't implement shrink");
    };

    BaseGenerator.prototype.suchThat = function (func) {
        return new FilteredGenerator(this, func);
    };
    return BaseGenerator;
})();
exports.BaseGenerator = BaseGenerator;

var FilteredGenerator = (function (_super) {
    __extends(FilteredGenerator, _super);
    function FilteredGenerator(wrapped, filter) {
        _super.call(this);
        this.wrapped = wrapped;
        this.filter = filter;
    }
    FilteredGenerator.prototype.arbitrary = function (rng, size) {
        var tryCounter = 0;
        var genValue = this.wrapped.arbitrary(rng, size);

        while (!this.filter(genValue)) {
            tryCounter++;
            if (tryCounter > 100) {
                throw new Error("Generator suchThat filter too restrictive");
            }

            genValue = this.wrapped.arbitrary(rng, size);
        }

        return genValue;
    };

    FilteredGenerator.prototype.shrink = function (value) {
        var shrinks = this.wrapped.shrink(value);
        shrinks = shrinks.filter(this.filter);
        return shrinks.length == 0 ? [value] : shrinks;
    };
    return FilteredGenerator;
})(BaseGenerator);
exports.FilteredGenerator = FilteredGenerator;

function map(gen, func) {
    return new MapGenerator(gen, func);
}
exports.map = map;

var MapGenerator = (function (_super) {
    __extends(MapGenerator, _super);
    function MapGenerator(wrapped, map) {
        _super.call(this);
        this.wrapped = wrapped;
        this.map = map;
    }
    MapGenerator.prototype.arbitrary = function (rng, size) {
        return this.map(this.wrapped.arbitrary(rng, size));
    };

    MapGenerator.prototype.shrink = function (value) {
        return this.wrapped.shrink(value).map(this.map);
    };
    return MapGenerator;
})(BaseGenerator);
exports.MapGenerator = MapGenerator;

function oneOf() {
    if (_.isArray(arguments[0])) {
        return new OneOfGenerator(arguments[0]);
    } else {
        return new OneOfGenerator(Array.prototype.slice.call(arguments, 0));
    }
    ;
}
exports.oneOf = oneOf;
;

function frequency() {
    if (_.isArray(arguments[0])) {
        return new FrequencyGenerator(arguments[0]);
    } else {
        return new FrequencyGenerator(Array.prototype.slice.call(arguments, 0));
    }
    ;
}
exports.frequency = frequency;
;

var FrequencyGenerator = (function (_super) {
    __extends(FrequencyGenerator, _super);
    function FrequencyGenerator(values) {
        var _this = this;
        _super.call(this);
        this.frequencies = values.map(function (v) {
            return v[0];
        });
        this.allotment = [];

        this.frequencyTotal = this.frequencies.reduce(function (acc, f1, ix) {
            var max = acc + f1;
            _this.allotment.push({
                min: acc, max: max, ix: ix
            });

            return max;
        }, 0);
        this.allotment[this.allotment.length - 1].max++;
        this.values = values.map(function (v) {
            return v[1];
        });
    }
    FrequencyGenerator.prototype.arbitrary = function (rng, size) {
        var target = Math.round(rng() * this.frequencyTotal);
        var ix = _.find(this.allotment, function (a) {
            return (a.min <= target) && (a.max > target);
        }).ix;
        return this.values[ix];
    };

    FrequencyGenerator.prototype.shrink = function (val) {
        return [val];
    };
    return FrequencyGenerator;
})(BaseGenerator);
exports.FrequencyGenerator = FrequencyGenerator;

var OneOfGenerator = (function (_super) {
    __extends(OneOfGenerator, _super);
    function OneOfGenerator(values) {
        _super.call(this, values.map(function (v) {
            return [1, v];
        }));
    }
    return OneOfGenerator;
})(FrequencyGenerator);
exports.OneOfGenerator = OneOfGenerator;

function arrayOf(gen, minSize, maxSize) {
    return new ArrayGenerator(gen, minSize, maxSize);
}
exports.arrayOf = arrayOf;

var ArrayGenerator = (function (_super) {
    __extends(ArrayGenerator, _super);
    function ArrayGenerator(gen, minSize, maxSize) {
        _super.call(this);
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.wrapped = gen;
        if (minSize < 0) {
            throw new Error("Arrays cannot be smaller than 0 length.");
        }

        if (minSize && maxSize && maxSize < minSize) {
            throw new Error("Maximum Size must be greater than minimum size.");
        }
        if (!minSize) {
            this.minSize = 0;
        }
    }
    ArrayGenerator.prototype.arbitrary = function (rng, size) {
        var _this = this;
        var upper = this.maxSize ? this.maxSize : size;
        var sizeMultiplier = size == 0 ? size : size / 100;
        var range = upper - this.minSize;
        var multi = rng() * sizeMultiplier;
        var arrayLength = this.minSize + ((rng() * sizeMultiplier) * range);

        return _.range(arrayLength).map(function (ix) {
            return _this.wrapped.arbitrary(rng, size);
        });
    };

    ArrayGenerator.prototype.shrink = function (value) {
        var _this = this;
        var shrinkCollections = _.range(value.length + 1).map(function (len) {
            var valShrinks = _.range(len).map(function (ix) {
                return _.shuffle(_this.wrapped.shrink(value[ix]));
            });

            var arrayZips = _.zip.apply(null, valShrinks);

            // remove empty cells
            return arrayZips.map(function (xs) {
                return xs.filter(function (x) {
                    return !_.isUndefined(x);
                });
            });
        });

        return _.flatten(shrinkCollections, true);
    };
    return ArrayGenerator;
})(BaseGenerator);
exports.ArrayGenerator = ArrayGenerator;

function sized(func) {
    return new SizedGenerator(func);
}
exports.sized = sized;

var SizedGenerator = (function (_super) {
    __extends(SizedGenerator, _super);
    function SizedGenerator(func) {
        _super.call(this);
        this.arbitraryFunc = func;
    }
    SizedGenerator.prototype.arbitrary = function (rng, size) {
        this.lastRng = rng;
        this.lastSize = size;
        return this.arbitraryFunc(rng, size);
    };

    SizedGenerator.prototype.shrink = function (value) {
        var shrinks = [];
        while (shrinks.length < 30) {
            try  {
                shrinks.push(this.arbitrary(this.lastRng, this.lastSize - 1));
            } catch (e) {
                break;
            }
        }

        return (shrinks.length < 1) ? [value] : shrinks;
    };
    return SizedGenerator;
})(BaseGenerator);
exports.SizedGenerator = SizedGenerator;

function isInteger(n) {
    return n === +n && n === (n | 0);
}
;

function chooseInt(min, max) {
    if (!isInteger(min) || !isInteger(max)) {
        throw new Error("ChooseInt expects integers as range values.");
    }

    return new ChooseGenerator(min, max, "integer");
}
exports.chooseInt = chooseInt;

function choose(min, max) {
    return new ChooseGenerator(min, max);
}
exports.choose = choose;

var ChooseGenerator = (function (_super) {
    __extends(ChooseGenerator, _super);
    function ChooseGenerator(min, max, modifier) {
        _super.call(this);
        this.min = min;
        this.max = max;
        this.modifier = modifier;
        if (typeof min != typeof max) {
            throw new Error("Cannot generate range between unlike types");
        }
        if (min > max) {
            throw new Error("Cannot generate range, min greater than max");
        }
        this.type = typeof min;

        if (this.type == "string" && (min.length > 1 || max.length > 1)) {
            throw new Error("Cannot create ranges between strings, only chars");
        }

        if (this.type == "string") {
            this.min = this.min.charCodeAt();
            this.max = this.max.charCodeAt();
        }
    }
    ChooseGenerator.prototype.arbitrary = function (rng, size) {
        switch (this.type) {
            case "number":
                if (this.modifier && this.modifier == "integer") {
                    return this.chooseInteger(rng, size);
                } else {
                    return this.chooseNumber(rng, size);
                }

            case "string":
                return this.chooseCharacter(rng, size);
        }
    };

    ChooseGenerator.prototype.shrink = function (value) {
        switch (this.type) {
            case "number":
                if (this.modifier && this.modifier == "integer") {
                    return this.shrinkInteger(value);
                } else {
                    return this.shrinkNumber(value);
                }

            case "string":
                return this.shrinkCharacter(value);
        }
    };

    ChooseGenerator.prototype.chooseNumber = function (rng, size) {
        var sizeMultiplier = size == 0 ? size : size / 100;
        var range = this.max - this.min;
        var multi = rng() * sizeMultiplier;
        var num = this.min + ((rng() * sizeMultiplier) * range);

        return num > this.max ? this.max : num;
    };

    ChooseGenerator.prototype.chooseInteger = function (rng, size) {
        return Math.round(this.chooseNumber(rng, size));
    };

    ChooseGenerator.prototype.chooseCharacter = function (rng, size) {
        var charCode = this.chooseInteger(rng, size);
        return String.fromCharCode(charCode);
    };

    ChooseGenerator.prototype.shrinkNumber = function (value) {
        var _this = this;
        var range = value - this.min;

        return _.range(30).map(function (factor) {
            return _this.min + (range * (factor / 30));
        });
    };

    ChooseGenerator.prototype.shrinkInteger = function (value) {
        // get result of shrinkNumber, round all and remove duplicates
        return _.uniq(this.shrinkNumber(value).map(function (val) {
            return Math.round(val);
        }));
    };

    ChooseGenerator.prototype.shrinkCharacter = function (value) {
        var charCodes = this.shrinkInteger(value.charCodeAt());
        return charCodes.map(function (c) {
            return String.fromCharCode(c);
        });
    };
    return ChooseGenerator;
})(BaseGenerator);
exports.ChooseGenerator = ChooseGenerator;

(function (Values) {
    function string() {
        return exports.map(exports.arrayOf(exports.choose(" ", "~")), function (val) {
            return val.join("");
        });
    }
    Values.string = string;

    function boolean() {
        return exports.oneOf(true, false);
    }
    Values.boolean = boolean;

    function date(min, max) {
        if (!min)
            min = new Date(0);
        if (!max)
            max = new Date(+new Date() + 100000000);

        return exports.map(exports.chooseInt((+min | 0), (+max | 0)), function (val) {
            return new Date(val);
        });
    }
    Values.date = date;

    var zero = 0;
    var bigNeg = -9007199254740992;
    var bigPos = 9007199254740992;
    var pos = 2147483647;
    var neg = -2147483648;
    var smallPos = 32767;
    var smallNeg = -32768;
    var tinyPos = 255;
    var tinyNeg = -255;

    function bigInt() {
        return exports.chooseInt(bigNeg, bigPos);
    }
    Values.bigInt = bigInt;
    function bigPositiveInt() {
        return exports.chooseInt(zero, bigPos);
    }
    Values.bigPositiveInt = bigPositiveInt;
    function bigNegativeInt() {
        return exports.chooseInt(bigNeg, zero);
    }
    Values.bigNegativeInt = bigNegativeInt;
    function smallInt() {
        return exports.chooseInt(smallNeg, smallPos);
    }
    Values.smallInt = smallInt;
    function smallPositiveInt() {
        return exports.chooseInt(zero, smallPos);
    }
    Values.smallPositiveInt = smallPositiveInt;
    function smallNegativeInt() {
        return exports.chooseInt(smallNeg, zero);
    }
    Values.smallNegativeInt = smallNegativeInt;
    function int() {
        return exports.chooseInt(neg, pos);
    }
    Values.int = int;
    function positiveInt() {
        return exports.chooseInt(zero, pos);
    }
    Values.positiveInt = positiveInt;
    function negativeInt() {
        return exports.chooseInt(neg, zero);
    }
    Values.negativeInt = negativeInt;
    function tinyInt() {
        return exports.chooseInt(tinyNeg, tinyPos);
    }
    Values.tinyInt = tinyInt;
    function tinyPositiveInt() {
        return exports.chooseInt(zero, tinyPos);
    }
    Values.tinyPositiveInt = tinyPositiveInt;
    function tinyNegativeInt() {
        return exports.chooseInt(tinyNeg, zero);
    }
    Values.tinyNegativeInt = tinyNegativeInt;

    function bigFloat() {
        return exports.choose(bigNeg, bigPos);
    }
    Values.bigFloat = bigFloat;
    function bigPositiveFloat() {
        return exports.choose(zero, bigPos);
    }
    Values.bigPositiveFloat = bigPositiveFloat;
    function bigNegativeFloat() {
        return exports.choose(bigNeg, zero);
    }
    Values.bigNegativeFloat = bigNegativeFloat;
    function smallFloat() {
        return exports.choose(smallNeg, smallPos);
    }
    Values.smallFloat = smallFloat;
    function smallPositiveFloat() {
        return exports.choose(zero, smallPos);
    }
    Values.smallPositiveFloat = smallPositiveFloat;
    function smallNegativeFloat() {
        return exports.choose(smallNeg, zero);
    }
    Values.smallNegativeFloat = smallNegativeFloat;
    function float() {
        return exports.choose(neg, pos);
    }
    Values.float = float;
    function positiveFloat() {
        return exports.choose(zero, pos);
    }
    Values.positiveFloat = positiveFloat;
    function negativeFloat() {
        return exports.choose(neg, zero);
    }
    Values.negativeFloat = negativeFloat;
    function tinyFloat() {
        return exports.choose(tinyNeg, tinyPos);
    }
    Values.tinyFloat = tinyFloat;
    function tinyPositiveFloat() {
        return exports.choose(zero, tinyPos);
    }
    Values.tinyPositiveFloat = tinyPositiveFloat;
    function tinyNegativeFloat() {
        return exports.choose(tinyNeg, zero);
    }
    Values.tinyNegativeFloat = tinyNegativeFloat;
})(exports.Values || (exports.Values = {}));
var Values = exports.Values;
