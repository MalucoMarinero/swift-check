///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Promise = require('bluebird');
var _ = require('lodash');
var colors = require('colors');

var Gen = require('./Generators');
var Prop = require('./Properties');

var CommandTestCase = (function (_super) {
    __extends(CommandTestCase, _super);
    function CommandTestCase() {
        _super.call(this, []);
        this.commandSequence = [];
    }
    CommandTestCase.prototype.addStep = function (name, state) {
        this.commandSequence.push({
            stepName: name, postState: state
        });
    };
    return CommandTestCase;
})(Prop.TestCase);
exports.CommandTestCase = CommandTestCase;

var PreconditionsExhausted = (function () {
    function PreconditionsExhausted(message) {
        this.message = message;
        this.name = "PreconditionsExhausted";
    }
    return PreconditionsExhausted;
})();
exports.PreconditionsExhausted = PreconditionsExhausted;

var CommandsProperty = (function (_super) {
    __extends(CommandsProperty, _super);
    function CommandsProperty(commandSetup) {
        _super.call(this);
        this.commands = {};
        this.invariants = [];
        commandSetup.call(this, this);
    }
    CommandsProperty.prototype.initialState = function (setupFunc) {
        this.setupFunc = setupFunc;
    };

    CommandsProperty.prototype.initialSUT = function (setupFunc) {
        this.setupSUT = setupFunc;
    };

    CommandsProperty.prototype.promised = function (b) {
        this.isPromised = b;
    };

    CommandsProperty.prototype.addCommand = function (name, run, nextState, extras) {
        var command = new Command(name, run, nextState);
        this.commands[name] = command;

        if (extras) {
            if (extras.pre || extras.preConditions) {
                var preConditions = extras.pre || extras.preConditions;
                if (_.isArray(preConditions)) {
                    preConditions.forEach(function (pre) {
                        return command.pre(pre);
                    });
                } else {
                    command.pre(preConditions);
                }
            }

            if (extras.post || extras.postConditions) {
                var postConditions = extras.post || extras.postConditions;
                if (_.isArray(postConditions)) {
                    postConditions.forEach(function (post) {
                        return command.post(post);
                    });
                } else {
                    command.post(postConditions);
                }
            }
        }

        return command;
    };

    CommandsProperty.prototype.invariant = function (invariant) {
        this.addInvariant(invariant);
    };
    CommandsProperty.prototype.addInvariant = function (invariant) {
        this.invariants.push(invariant);
    };

    CommandsProperty.prototype.shrinkTestCase = function (testCase, explore) {
        var maxLength = testCase.commandSequence.length;
        var commands = _.values(this.commands);

        if (!explore && maxLength > 7)
            maxLength = 7;

        var possibleCombinations = Math.pow(commands.length, maxLength);

        var iterator = _.range(maxLength).map(function (xs) {
            return -1;
        });

        var tries = 0;
        var ceiling = 0;
        while (iterator.some(function (cIx, ix) {
            return cIx < commands.length - 1;
        })) {
            tries++;

            // advance first iterator
            iterator[0]++;
            for (var ix = 0; ix < iterator.length; ix++) {
                if (iterator[ix] == commands.length && ix == ceiling) {
                    ceiling++;
                }
                if (iterator[ix] == commands.length) {
                    iterator[ix] = 0;
                    if (ix + 1 < iterator.length)
                        iterator[ix + 1]++;
                }
            }

            // run testCase
            var nextTestCase = new CommandTestCase();
            var state = this.setupFunc.apply(nextTestCase);
            var SUT = this.setupSUT.apply(nextTestCase);
            var result = true;

            for (var ix = 0; ix < ceiling + 1; ix++) {
                var nextCom = commands[iterator[ix]];

                var nextSUT = nextCom.run.apply(nextTestCase, [SUT]);
                var nextState = nextCom.nextState.apply(nextTestCase, [state]);
                nextTestCase.addStep(nextCom.name, nextState);

                nextCom.postConditions.forEach(function (post) {
                    if (post.apply(testCase, [state, nextState, nextSUT])) {
                        return;
                    }
                    result = false;
                    nextTestCase.label("postCondition failed: " + post.toString());
                });

                this.invariants.forEach(function (inv) {
                    if (inv.apply(nextTestCase, [nextState, nextSUT]))
                        return;
                    result = false;
                    nextTestCase.label("invariant failed: " + inv.toString());
                });

                if (!result) {
                    break;
                } else {
                    SUT = nextSUT;
                    state = nextState;
                }
            }

            nextTestCase.args = [nextTestCase.commandSequence.map(function (c) {
                    return c.stepName;
                })];
            if (!result) {
                nextTestCase.setResult(result);
                return nextTestCase;
            }
        }

        // exhausted possibilities
        return testCase;
    };

    CommandsProperty.prototype.promiseShrinkTestCase = function (testCase, explore) {
        var _this = this;
        var maxLength = testCase.commandSequence.length;
        var commands = _.values(this.commands);

        if (!explore && maxLength > 7)
            maxLength = 7;

        var possibleCombinations = Math.pow(commands.length, maxLength);

        var iterator = _.range(maxLength).map(function (xs) {
            return -1;
        });

        var tries = 0;
        var sprintEnd = 2;
        var ceiling = 0;
        var iterations = [];
        var doSprint = function () {
            while (iterator.some(function (cIx, ix) {
                return cIx < commands.length - 1;
            })) {
                if (ceiling == sprintEnd)
                    break;
                tries++;

                // advance first iterator
                iterator[0]++;
                for (var ix = 0; ix < iterator.length; ix++) {
                    if (iterator[ix] == commands.length && ix == ceiling) {
                        ceiling++;
                    }
                    if (iterator[ix] == commands.length) {
                        iterator[ix] = 0;
                        if (ix + 1 < iterator.length)
                            iterator[ix + 1]++;
                    }
                }

                iterations.push(new Promise(function (resolve, reject) {
                    // run testCase
                    var nextTestCase = new CommandTestCase();
                    var state = _this.setupFunc.apply(nextTestCase);
                    var SUT = _this.setupSUT.apply(nextTestCase);
                    var result = true;

                    var doStep = function (ix, testState) {
                        if (testState == false)
                            return Promise.resolve(false);
                        var nextCom = commands[iterator[ix]];

                        if (nextCom.run.length == 2) {
                            // call with callback
                            var systemPromise = new Promise(function (res, rej) {
                                var next = nextCom;
                                nextCom.run.apply(nextTestCase, [
                                    testState.sut, function (newSUT) {
                                        res(newSUT);
                                    }
                                ]);
                            });
                        } else {
                            var systemPromise = Promise.resolve(nextCom.run.apply(nextTestCase, [testState.sut]));
                        }

                        return systemPromise.then(function (nextSUT) {
                            var nextState = nextCom.nextState.apply(nextTestCase, [testState.state]);
                            nextTestCase.addStep(nextCom.name, nextState);

                            nextCom.postConditions.forEach(function (post) {
                                if (post.apply(testCase, [testState.state, nextState, nextSUT])) {
                                    return;
                                }
                                result = false;
                                testCase.label("postCondition failed: " + post.toString());
                            });

                            _this.invariants.forEach(function (inv) {
                                if (inv.apply(testCase, [nextState, nextSUT])) {
                                    return;
                                }
                                result = false;
                                testCase.label("invariant failed: " + inv.toString());
                            });

                            if (!result) {
                                return false;
                            } else {
                                return {
                                    sut: nextSUT,
                                    state: nextState
                                };
                            }
                        });
                    };

                    var testPromise = Promise.resolve({
                        sut: SUT,
                        state: state
                    });

                    for (var i = 0; i < ceiling + 1; i++) {
                        testPromise = testPromise.then(doStep.bind(undefined, i));
                    }
                    ;

                    return resolve(testPromise.then(function (finalState) {
                        // if (discard) continue;
                        nextTestCase.args = [nextTestCase.commandSequence.map(function (c) {
                                return c.stepName;
                            })];

                        nextTestCase.setResult(result);

                        return nextTestCase;
                    }));
                }));

                return Promise.settle(iterations).then(function (resultSet) {
                    for (var ix = 0; ix < resultSet.length; ix++) {
                        var pi = resultSet[ix];

                        if (pi.isRejected()) {
                            if (pi._settledValue.name == 'ImplicationFailure') {
                                continue;
                            } else {
                                throw pi._settledValue;
                            }
                        } else {
                            var nextTestCase = pi.value();
                        }

                        if (nextTestCase.getResult()) {
                            continue;
                        } else {
                            return nextTestCase;
                        }
                    }
                    ;

                    // exhausted possibilities
                    if (ceiling < maxLength) {
                        sprintEnd++;
                        return doSprint();
                    } else {
                        return testCase;
                    }
                });
            }
            ;
        };

        return doSprint();
    };

    CommandsProperty.prototype.nextCommand = function (p, size, state) {
        var next;
        var valid = false;
        var tries = 0;
        while (!valid) {
            tries++;
            if (tries > 10) {
                var fails = next.preConditions.filter(function (pre) {
                    return !pre(state);
                });
                var failOutput = "Preconditions not satisfied after 50 tries.\n";
                failOutput += fails.map(function (f) {
                    return f.toString();
                }).join("\n");
                valid = next.preConditions.every(function (pre) {
                    return pre(state);
                });
                throw new PreconditionsExhausted(failOutput);
            }

            var commandGen = Gen.oneOf(_.values(this.commands));
            next = commandGen.arbitrary(p.rng, size);

            valid = next.preConditions.every(function (pre) {
                return pre(state);
            });
        }

        return next;
    };

    CommandsProperty.prototype.run = function (p) {
        if (_.values(this.commands).length == 0) {
            throw new Error("Property can't run: no commands specified");
        }

        p = p || {};
        _.defaults(p, Prop.DefaultRunParameters);

        if (this.isPromised)
            return this.runPromised(p);

        var stepSize = (p.maxSize - p.minSize) / p.minTestPasses;

        var results = [];
        var passes = 0;
        var discards = 0;
        var currentSize = 0;
        var collectedTestData = {};

        var discardRatioPassed = function () {
            return (discards + passes > p.minTestPasses && discards / p.maxDiscardRatio >= p.minTestPasses);
        };

        while (passes < p.minTestPasses) {
            if (discardRatioPassed()) {
                break;
            }

            var testCase = new CommandTestCase();
            var state = this.setupFunc.apply(testCase);
            var SUT = this.setupSUT.apply(testCase);
            var discard = false;
            var result = true;

            while (testCase.commandSequence.length < currentSize) {
                try  {
                    var nextCom = this.nextCommand(p, currentSize, state);
                } catch (e) {
                    if (e.name == "PreconditionsExhausted") {
                        discards++;
                        currentSize += stepSize;
                        discard = true;
                        break;
                    } else {
                        throw e;
                    }
                }

                var nextSUT = nextCom.run.apply(testCase, [SUT]);
                var nextState = nextCom.nextState.apply(testCase, [state]);
                testCase.addStep(nextCom.name, nextState);

                nextCom.postConditions.forEach(function (post) {
                    if (post.apply(testCase, [state, nextState, nextSUT])) {
                        return;
                    }
                    ;
                    result = false;
                    testCase.label("postCondition failed: " + post.toString());
                });

                this.invariants.forEach(function (inv) {
                    if (inv.apply(testCase, [nextState, nextSUT])) {
                        return;
                    }
                    result = false;
                    testCase.label("invariant failed: " + inv.toString());
                });

                if (!result) {
                    break;
                } else {
                    SUT = nextSUT;
                    state = nextState;
                }
            }

            if (discard)
                continue;
            testCase.args = [testCase.commandSequence.map(function (c) {
                    return c.stepName;
                })];

            testCase.setResult(result);
            results.push(testCase);

            if (!testCase.getResult()) {
                break;
            } else {
                currentSize += stepSize;
                passes++;
            }
        }

        // return result of testing
        if (discardRatioPassed()) {
            return new Prop.Result(1 /* Undecided */, passes, discards, collectedTestData);
        } else if (passes < p.minTestPasses) {
            var failCase = _.find(results, function (res) {
                return !res.result;
            });
            var shrunkFailCase = this.shrinkTestCase(failCase, p.explore);

            return new Prop.Result(0 /* Failed */, passes, discards, collectedTestData, shrunkFailCase);
        } else {
            return new Prop.Result(2 /* Passed */, passes, discards, collectedTestData);
        }
    };

    CommandsProperty.prototype.runPromised = function (p) {
        var _this = this;
        var stepSize = (p.maxSize - p.minSize) / p.minTestPasses;

        var results = [];
        var sprintProgress = 0;
        var sprintEnd = p.minTestPasses;
        var passes = 0;
        var discards = 0;
        var currentSize = 0;
        var collectedTestData = {};

        var discardRatioPassed = function () {
            return (discards + passes > p.minTestPasses && discards / p.maxDiscardRatio >= p.minTestPasses);
        };

        var doSprint = function () {
            while (sprintProgress < sprintEnd) {
                if (discardRatioPassed()) {
                    break;
                }

                results.push(new Promise(function (resolve, reject) {
                    var testCase = new CommandTestCase();
                    var state = _this.setupFunc.apply(testCase);
                    var SUT = _this.setupSUT.apply(testCase);
                    var size = currentSize;
                    var discard = false;
                    var result = true;

                    var doStep = function (testState) {
                        if (testState == false)
                            return false;
                        try  {
                            var nextCom = _this.nextCommand(p, size, testState.state);
                        } catch (e) {
                            if (e.name == "PreconditionsExhausted") {
                                discards++;
                                size += stepSize;
                                discard = true;
                                return reject(e);
                            } else {
                                throw e;
                            }
                        }

                        if (nextCom.run.length == 2) {
                            // call with callback
                            var systemPromise = new Promise(function (res, rej) {
                                var next = nextCom;
                                next.run.apply(testCase, [
                                    testState.sut, function (newSUT) {
                                        res(newSUT);
                                    }
                                ]);
                            });
                        } else {
                            var systemPromise = Promise.resolve(nextCom.run.apply(testCase, [testState.sut]));
                        }

                        return systemPromise.then(function (nextSUT) {
                            var nextState = nextCom.nextState.apply(testCase, [testState.state]);
                            testCase.addStep(nextCom.name, nextState);

                            nextCom.postConditions.forEach(function (post) {
                                if (post.apply(testCase, [testState.state, nextState, nextSUT])) {
                                    return;
                                }
                                result = false;
                                testCase.label("postCondition failed: " + post.toString());
                            });

                            _this.invariants.forEach(function (inv) {
                                if (inv.apply(testCase, [nextState, nextSUT])) {
                                    return;
                                }
                                result = false;
                                testCase.label("invariant failed: " + inv.toString());
                            });

                            if (!result) {
                                return false;
                            } else {
                                return {
                                    sut: nextSUT,
                                    state: nextState
                                };
                            }
                        });
                    };

                    var testPromise = Promise.resolve({
                        sut: SUT,
                        state: state
                    });

                    for (var i = 0; i < size; i++) {
                        testPromise = testPromise.then(doStep);
                    }
                    ;

                    return resolve(testPromise.then(function (finalState) {
                        // if (discard) continue;
                        testCase.args = [testCase.commandSequence.map(function (c) {
                                return c.stepName;
                            })];

                        testCase.setResult(result);

                        return testCase;
                    }));
                }));

                currentSize += stepSize;
                sprintProgress++;
            }
            ;

            return Promise.settle(results).then(function (resultSet) {
                var collectedTestData = {};
                var passes = 0;
                var discardRatioPassed = function () {
                    return (discards + passes > p.minTestPasses && discards / p.maxDiscardRatio >= p.minTestPasses);
                };

                for (var ix = 0; ix < resultSet.length; ix++) {
                    var pi = resultSet[ix];

                    if (pi.isRejected()) {
                        if (pi._settledValue.name == 'PreconditionsExhausted') {
                            discards++;
                            continue;
                        } else {
                            throw pi._settledValue;
                        }
                    } else {
                        var testCase = pi.value();
                    }

                    if (!testCase.getResult()) {
                        break;
                    } else {
                        var classString = testCase.getClassificationString();
                        if (classString) {
                            if (_.isUndefined(collectedTestData[classString])) {
                                collectedTestData[classString] = 0;
                            }
                            collectedTestData[classString]++;
                        }

                        passes++;
                    }
                }
                ;

                var failCase = _.find(resultSet, function (res) {
                    return res.isRejected() ? false : !res.value().result;
                });

                // return result of testing
                if (discardRatioPassed()) {
                    return new Prop.Result(1 /* Undecided */, passes, discards, collectedTestData);
                } else if (failCase) {
                    // promised not shrinking for now
                    // var shrunkFailCase = this.shrinkTestCase(failCase);
                    var pR = _this.promiseShrinkTestCase(failCase.value());
                    return pR.then(function (shrunk) {
                        return new Prop.Result(0 /* Failed */, passes, discards, collectedTestData, shrunk);
                    });
                } else if (passes >= p.minTestPasses) {
                    return new Prop.Result(2 /* Passed */, passes, discards, collectedTestData);
                } else {
                    sprintEnd += p.minTestPasses;
                    return doSprint();
                }
            });
        };

        return doSprint();
    };
    return CommandsProperty;
})(Prop.BaseProperty);
exports.CommandsProperty = CommandsProperty;

var Command = (function () {
    function Command(name, run, nextState) {
        this.name = name;
        this.run = run;
        this.nextState = nextState;
        this.preConditions = [];
        this.postConditions = [];
    }
    Command.prototype.pre = function (pre) {
        this.preCondition(pre);
    };
    Command.prototype.preCondition = function (pre) {
        this.preConditions.push(pre);
    };

    Command.prototype.post = function (post) {
        this.postCondition(post);
    };
    Command.prototype.postCondition = function (post) {
        this.postConditions.push(post);
    };
    return Command;
})();
exports.Command = Command;
