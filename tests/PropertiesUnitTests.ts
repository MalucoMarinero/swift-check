///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/sinon.d.ts"/>
///<reference path="../typedefs/chai.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>

var Promise: Bluebird.PromiseStatic = require('bluebird');

import Prop = require("../src/Properties");
import Gen = require("../src/Generators");
import PropRunner = require("../src/PropRunner");
declare var describe, it, before; // Mocha injections.
var expect = require("chai").expect;
var sinon = require("sinon");
var _ = require('lodash');

var genInteger = {
    arbitrary: (rng, size) => Math.floor(rng() * size),
    shrink: (num) => _.range(num)
};


describe('Property To Generator Collaboration', () => {
    it('should call generator once per test', () => {
        var spy = sinon.spy(genInteger, "arbitrary");
        var p1 = Prop.forAll(genInteger, (n) => n == n);

        p1.run();
        expect(spy.callCount).to.equal(100);
        spy.restore();
    });

    it('should call each generator with RNG and Size', () => {
        var spy = sinon.spy(genInteger, "arbitrary");

        var p1 = Prop.forAll(genInteger, (n) => n == n);

        p1.run();
        expect(_.flatten(spy.args).length).to.equal(200);
        spy.restore();
    });

    it('should call with increasing size arguments', () => {
        // size progression up to maxSize
        var spy = sinon.spy(genInteger, "arbitrary");

        var p1 = Prop.forAll(genInteger, (n) => n == n);

        p1.run();
        var sizeArgs = spy.args.map((args) => args[1]);
        var sizeTotal = sizeArgs.reduce((x, y) => {
            return x + y;
        }, 0);
        expect(sizeTotal).to.equal(4950);
        spy.restore();
    });

    it('should call shrink on a property failure', () => {
        var spy = sinon.spy(genInteger, "shrink");
        var p1 = Prop.forAll(
            genInteger, (n: number) => n * 2 == n
        )

        p1.run();
        expect(spy.called).to.be.true;
        spy.restore();
    });

    it('should call shrink on a promised property failure', (done) => {
        var spy = sinon.spy(genInteger, "shrink");
        var p1 = Prop.forAll(
            genInteger, (n: number) => Promise.resolve(n * 2 == n)
        )

        p1.run().then((res) => {
            expect(spy.called).to.be.true;
            spy.restore();
            done();
        }).catch(done);
    });

    it('should call shrink once per generator on failure', () => {
        var spy = sinon.spy(genInteger, "shrink");

        var p1 = Prop.forAll(
            genInteger, genInteger, (n: number, x: number) => n * x == n + x
        )

        p1.run();
        expect(spy.calledTwice).to.be.true;
        spy.restore();
    });
});


describe("Property to PropRunner Collaboration", () => {
    it("calls PropRunner.check() via prop.check()", () => {
        var p1 = new Prop.QuantifiedProperty(
            genInteger, (n: number) => n == n
        )

        var stub = sinon.stub(PropRunner, "check");
        stub.returns(p1.run());

        p1.check();
        expect(stub.called).to.be.true;
        stub.restore();
    });

    it("calls PropRunner.checkProp() via prop.checkData()", () => {
        var p1 = new Prop.QuantifiedProperty(
            genInteger, (n: number) => n == n
        )

        var stub = sinon.stub(PropRunner, "checkProp");
        stub.returns(p1.run());

        p1.checkData();
        expect(stub.called).to.be.true;
        stub.restore();
    });
});


describe("Property to PropRunner Contract", () => {
    it("returns a Result on run()", () => {
        var p1 = new Prop.QuantifiedProperty(
            genInteger, (n: number) => n * 2 == n
        )

        var result = p1.run();
        expect(result).to.be.instanceof(Prop.Result);
    });

    it("returns a Promise Result on a promised run()", (done) => {
        var p1 = new Prop.QuantifiedProperty(
            genInteger, (n: number) => {
                return Promise.resolve(n * 2 == n);
            }
        )

        var result = p1.run();
        result.then((res) => {
            expect(res).to.be.instanceof(Prop.Result);
            done();
        }).catch(done);
    });

    it("returns a GroupResult on PropertyGroup.run()", () => {
        var group = Prop.group("StringSpec");

        group.add("sliceEq", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.slice(0) === str;
        }));

        group.add("lengthX", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.length == str.length + 2;
        }));

        var result = group.run();
        expect(result).to.be.instanceof(Prop.GroupResult);
    });
});




describe("Basic Property Results", () => {
    it("Result should be Passed on successful run", () => {
        var p1 = Prop.forAll(
            genInteger, (n: number) => n * 2 == n + n
        )

        var result = p1.run();
        expect(result.status).to.equal(Prop.Status.Passed);
    });

    it("check() should throw an Prop.PropertyFailure on failure", () => {
        var p1 = Prop.forAll(
            genInteger, (n: number) => n * 2 == n
        )

        var result = p1.run();
        expect(result.status).to.equal(Prop.Status.Failed);
    });

    it("returned error testCases should be shrunk to smallest case", () => {
        var error;
        var p1 = Prop.forAll(
            genInteger, (n: number) => n * 2 == n
        )

        var result = p1.run();
        expect(result.failCase.getArgs()[0]).to.equal(1)
    });

    it("forAllNoShrink does not shrink failed values", () => {
        var error;
        var p1 = Prop.forAllNoShrink(
            genInteger, (n: number) => Math.random() * n < 10
        )
        var p2 = Prop.forAll(
            genInteger, (n: number) => Math.random() * n < 10
        )

        var spy = sinon.spy(p1, 'shrinkTestCase');

        _.range(400).forEach(() => {
            var result = p1.run();

            // value returned by shrinkTestCase same as entered
            expect(spy.args[0][0]).to.equal(spy.returnValues[0]);
            spy.reset();
        });
        spy.restore();
    });
});

describe("Promised Property Results", () => {
    it("Result should be promised if given a promise function", () => {
        var p1 = Prop.forAll(
            genInteger, (n: number) => {
                return Promise.resolve(n * 2 == n + n);
            }
        )

        var result = p1.run();
        expect(typeof result['then']).to.equal('function');
    });

    it("result promise must contain a result", (done) => {
        var p1 = Prop.forAll(
            genInteger, (n: number) => {
                return Promise.resolve(n * 2 == n + n);
            }
        )

        var result = p1.run();
        result.then((res) => {
            expect(res.status).to.equal(Prop.Status.Passed);
            done();
        }).catch(done);
    });

    it("should be Undecided for implications that discard too much", (done) => {
        var error;
        var p1 = Prop.forAll(genInteger, function(n: number) {
            this.implies(n == 0); // zero only
            return Promise.resolve(n * 2 == n + n);
        });

        var result = p1.run();
        result.then((res) => {
            expect(res.status).to.equal(Prop.Status.Undecided);
            done();
        }).catch(done);
    });

    it("should be Undecided for implications inside promises", (done) => {
        var error;
        var p1 = Prop.forAll(genInteger, function(n: number) {
            return new Promise((resolve, reject) => {
                this.implies(n == 0); // zero only
                resolve(n * 2 == n + n);
            });
        });

        var result = p1.run();
        result.then((res) => {
            expect(res.status).to.equal(Prop.Status.Undecided);
            done();
        }).catch(done);
    });

    it("throwing other errors bubbles up to result promise", (done) => {
        var error;
        var p1 = Prop.forAll(genInteger, function(n: number) {
            return new Promise((resolve, reject) => {
                if (n > 20) {
                    throw new Error("This is broken");
                };
                resolve(n * 2 == n + n);
            });
        });

        var result = p1.run();
        result.then((res) => {
            done("Didn't expect to fall through successfully");
        }).catch((error) => {
            expect(error.message).to.contain("This is broken");
            done();
        });
    });

    // passed but bubbles up, need to investigate prevention.
    // it("immediate errors bubble up in promises", (done) => {
    //     var p1 = Prop.forAll(genInteger, function(n: number) {
    //         return new Promise((resolve, reject) => {
    //             throw new Error("This is broken");
    //             resolve(n * 2 == n + n);
    //         });
    //     });

    //     p1.run().then((res) => {
    //         done("Didn't expect to fall through successfully");
    //     }).catch((error) => {
    //         expect(error.message).to.contain("This is broken");
    //         done();
    //     });
    // });
    
    it("combine promised properties correctly", (done) => {
        var p1 = Prop.forAll(
            genInteger, (n: number) => {
                return Promise.resolve(n * 2 == n + n);
            }
        )

        var p2 = Prop.forAll(
            genInteger, (n: number) => {
                return Promise.resolve(n * 2 == n + n);
            }
        )

        var p3 = p1.and(p2);
        var result = p3.run();
        result.then((res) => {
            expect(res.status).to.equal(Prop.Status.Passed);
            done();
        }).catch(done);
    });

    it("combine promised properties correctly when passing OR", (done) => {
        var p1 = Prop.forAll(
            genInteger, (n: number) => {
                return Promise.resolve(n * 2 == n + n);
            }
        )
        
        var p2 = Prop.forAll(
            genInteger, (n: number) => {
                return Promise.resolve(n * 2 == n - n);
            }
        )

        var p3 = p1.or(p2);
        var result = p3.run();
        result.then((res) => {
            expect(res.status).to.equal(Prop.Status.Passed);
            done();
        }).catch(done);
    });

    it("combine promised properties correctly when failing AND", (done) => {
        var p1 = Prop.forAll(
            genInteger, (n: number) => {
                return Promise.resolve(n * 2 == n + n);
            }
        )
        
        var p2 = Prop.forAll(
            genInteger, (n: number) => {
                return Promise.resolve(n * 2 == n - n);
            }
        )

        var p3 = p1.and(p2);
        var result = p3.run();
        result.then((res) => {
            expect(res.status).to.equal(Prop.Status.Failed);
            done();
        }).catch(done);
    });
});


describe("Combining Properties", () => {
    var passProp1 = Prop.forAll(genInteger, (n: number) => n * 2 == n + n);
    var passProp2 = Prop.forAll(genInteger, (n: number) => n * 2 == n + n);
    var failProp1 = Prop.forAll(genInteger, (n: number) => n * 2 == n + n + 1);
    var failProp2 = Prop.forAll(genInteger, (n: number) => n * 2 == n + n + 1);
    var impliedProp = Prop.forAll(genInteger, function(n: number) {
        this.implies(n % 2 == 0); // even numbers only
        return n * 2 == n + n;
    });
    var undecidedProp = Prop.forAll(genInteger, function(n: number) {
        this.implies(n == 0); // zero only
        return n * 2 == n + n;
    });

    var testMatrix = [
        [passProp1, "and", passProp2, Prop.Status.Passed],
        [failProp1, "and", passProp2, Prop.Status.Failed],
        [passProp1, "and", failProp1, Prop.Status.Failed],
        [undecidedProp, "and", passProp2, Prop.Status.Undecided],
        [undecidedProp, "and", undecidedProp, Prop.Status.Undecided],
        [passProp1, "and", undecidedProp, Prop.Status.Undecided],
        [failProp2, "and", failProp1, Prop.Status.Failed],
        [undecidedProp, "and", failProp1, Prop.Status.Failed],

        [passProp1, "or", passProp2, Prop.Status.Passed],
        [failProp1, "or", passProp2, Prop.Status.Passed],
        [passProp1, "or", failProp2, Prop.Status.Passed],
        [failProp1, "or", failProp2, Prop.Status.Failed],
        [undecidedProp, "or", passProp1, Prop.Status.Passed],
        [undecidedProp, "or", failProp1, Prop.Status.Undecided],
        [undecidedProp, "or", undecidedProp, Prop.Status.Undecided],
    ];

    var S = Prop.Status;

    testMatrix.forEach((line: any) => {
        var p1 = line[0];
        var op = line[1];
        var p2 = line[2];
        var expectStatus = line[3];

        var specTitle = "p1." + op + "(p2) should ";
        specTitle += "have status " + S[expectStatus] + " when ";
        specTitle += "p1:" + S[p1.run().status] + " and ";
        specTitle += "p2:" + S[p2.run().status];

        it(specTitle, () => {
            var result = p1[op](p2).run();
            expect(result.status).to.equal(expectStatus);
        });
    });


    it("combined result passes should equal sum of passes", () => {
        var p3 = passProp1.and(passProp2);

        var result = p3.run();
        expect(result.passes).to.equal(
            passProp1.run().passes + passProp2.run().passes
        );
    });

    it("combined result discards should equal sum of discards", () => {
        var p3 = passProp1.and(impliedProp);

        var result = p3.run();
        expect(result.passes).to.equal(
            passProp1.run().passes + impliedProp.run().passes
        );
    });

    it("Prop.all(p1, p2, p3) passes if all props pass", () => {
        var pAll = Prop.all(passProp1, passProp2, passProp1);

        var result = pAll.run();
        expect(result.status).to.equal(Prop.Status.Passed);
    });

    it("Prop.all(p1, p2, p3) fails if one props fais", () => {
        var pAll = Prop.all(passProp1, passProp2, failProp1);

        var result = pAll.run();
        expect(result.status).to.equal(Prop.Status.Failed);
    });

    it("Prop.atLeastOne(p1, p2, p3) passes if all props pass", () => {
        var pAll = Prop.atLeastOne(passProp1, passProp2, passProp1);

        var result = pAll.run();
        expect(result.status).to.equal(Prop.Status.Passed);
    });

    it("Prop.atLeastOne(p1, p2, p3) passes if one props passes", () => {
        var pAll = Prop.atLeastOne(passProp1, failProp2, failProp1);

        var result = pAll.run();
        expect(result.status).to.equal(Prop.Status.Passed);
    });
    
    it("Prop.atLeastOne(p1, p2, p3) fails if all props fail", () => {
        var pAll = Prop.atLeastOne(failProp1, failProp2, failProp1);

        var result = pAll.run();
        expect(result.status).to.equal(Prop.Status.Failed);
    });
    
});




describe("Implications on Properties", () => {
    it("should support implications", () => {
        var error;
        var p1 = Prop.forAll(genInteger, function(n: number) {
            this.implies(n % 2 == 0); // even numbers only
            return n * 2 == n + n;
        });

        p1.run();
    });

    it("should throw PropertyUndecided for implications that discard too much", () => {
        var error;
        var p1 = Prop.forAll(genInteger, function(n: number) {
            this.implies(n == 0); // zero only
            return n * 2 == n + n;
        });

        var result = p1.run();
        expect(result.status).to.equal(Prop.Status.Undecided);
    });
});


describe("Classification on Properties", () => {
    it("should collect classifiers in result test data", () => {
        var p1 = Prop.forAll(genInteger, function(n: number) {
            this.classify(n > 20, "above twenty");
            return n * 2 == n + n;
        });

        var result = p1.run();
        expect(result.testData["above twenty"]).to.exist;
    });

    it("should support true/false classifications", () => {
        var p1 = Prop.forAll(genInteger, function(n: number) {
            this.classify(n % 2 == 0, "even", "odd");
            return n * 2 == n + n;
        });

        var result = p1.run();
        expect(result.testData).to.include.keys('even', 'odd');
    });

    it("should support general collection of data", () => {
        var p1 = Prop.forAll(genInteger, function(n: number) {
            this.collect(n == 0 ? 0 : Math.floor(n / 10));
            return n * 2 == n + n;
        });

        var result = p1.run();
        expect(Object.keys(result.testData)).to.have.length.above(0);
    });
});


describe("Labelling Failed Properties", () => {
    it("should label property test cases", () => {
        var p1 = Prop.forAll(genInteger, function(n: number) {
            this.label("n*n == " + (n*n));
            return n * 2 == n;
        });

        var result = p1.run();
        var arg = result.failCase.getArg(0);
        expect(result.failCase.getLabels()[0]).to.equal("n*n == " + (arg*arg));
    });

    it(": labels with boolean arguments should only show when true", () => {
        var p1 = Prop.forAll(genInteger, function(n: number) {
            this.label("true label", true);
            this.label("false label", false);
            return n * 2 == n;
        });

        var result = p1.run();
        expect(result.failCase.getLabels()[0]).to.equal("true label");
        expect(result.failCase.getLabels().length).to.equal(1);
    });

    it(": labels should return their boolean value", () => {
        var p1 = Prop.forAll(genInteger, function(n: number) {
            return this.label("should pass", true);
        });

        var result = p1.run();
        expect(result.status).to.equal(Prop.Status.Passed);
    });
});


describe("PropertyGroup", () => {
    it("can create an empty PropertyGroup with Prop.group()", () => {
        var group = Prop.group("StringSpec");
    });

    it("can add properties using add()", () => {
        var group = Prop.group("StringSpec");

        group.add("sliceEq", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.slice(0) === str;
        }));
    });

    it("can test property groups, passing if all pass", () => {
        var group = Prop.group("StringSpec");

        group.add("sliceEq", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.slice(0) === str;
        }));

        group.add("length", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.length == str.length;
        }));

        var result = group.run();
        expect(result.status).to.equal(Prop.Status.Passed);
    });

    it("can test property groups, undecided if any undecided", () => {
        var group = Prop.group("StringSpec");

        group.add("sliceEq", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.slice(0) === str;
        }));

        group.add("length", Prop.forAll(Gen.Values.string(), function(str: any) {
            this.implies(str.length == 0);
            return str.length == str.length;
        }));

        var result = group.run();
        expect(result.status).to.equal(Prop.Status.Undecided);
    });

    it("can test property groups, failing if one fails", () => {
        var group = Prop.group("StringSpec");

        group.add("sliceEq", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.slice(0) === str;
        }));

        group.add("lengthX", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.length == str.length + 2;
        }));

        var result = group.run();
        expect(result.status).to.equal(Prop.Status.Failed);
    });

    it("property group result's pass count == number of props passed", () => {
        var group = Prop.group("StringSpec");

        group.add("sliceEq", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.slice(0) === str;
        }));

        group.add("length", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.length == str.length;
        }));

        var result = group.run();
        expect(result.passes).to.equal(2);
    });

    it("property group result allows inspection of sub props", () => {
        var group = Prop.group("StringSpec");

        group.add("sliceEq", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.slice(0) === str;
        }));

        group.add("length", Prop.forAll(Gen.Values.string(), (str: any) => {
            return str.length == str.length;
        }));

        var result = group.run();
        expect(_.values(result.results).length).to.equal(2);
        expect(result.results["sliceEq"]).to.be.instanceof(Prop.Result);
        expect(result.results["length"]).to.be.instanceof(Prop.Result);
    });
});
