///<reference path="../typedefs/node.d.ts"/>
///<reference path="../typedefs/bluebird.d.ts"/>
///<reference path="../typedefs/lodash.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Promise = require('bluebird');
var _ = require('lodash');
var colors = require('colors');

var PropRunner = require('./PropRunner');

(function (Status) {
    Status[Status["Failed"] = 0] = "Failed";
    Status[Status["Undecided"] = 1] = "Undecided";
    Status[Status["Passed"] = 2] = "Passed";
})(exports.Status || (exports.Status = {}));
var Status = exports.Status;

var Result = (function () {
    function Result(status, passes, discards, testData, failCase, results) {
        this.status = status;
        this.passes = passes;
        this.discards = discards;
        this.testData = testData;
        this.failCase = failCase;
        this.results = results;
    }
    Result.prototype.combine = function (status, results) {
        var mergedData = _.clone(this.testData);
        var passes = this.passes;
        var discards = this.discards;
        results.forEach(function (result) {
            passes += result.passes;
            discards += result.discards;

            _.forOwn(result.testData, function (count, label) {
                if (mergedData[label]) {
                    mergedData[label] = 0;
                }
                mergedData[label] += count;
            });
        });

        var failCase = status == 0 /* Failed */ ? this.failCase || _.find(results, function (r) {
            return r.status == 0 /* Failed */;
        }).failCase : null;

        return new Result(status, passes, discards, mergedData, failCase);
    };
    return Result;
})();
exports.Result = Result;

var GroupResult = (function (_super) {
    __extends(GroupResult, _super);
    function GroupResult(status, passes, discards, testData, failCase, results) {
        _super.call(this, status, passes, discards, testData, failCase, results);
        this.status = status;
        this.passes = passes;
        this.discards = discards;
        this.testData = testData;
        this.failCase = failCase;
        this.results = results;
    }
    return GroupResult;
})(Result);
exports.GroupResult = GroupResult;

// thrown on an implication test failing to prevent running
// the singular case any further than necessary
var ImplicationFailure = (function () {
    function ImplicationFailure(message) {
        this.message = message;
        this.name = "ImplicationFailure";
    }
    return ImplicationFailure;
})();
exports.ImplicationFailure = ImplicationFailure;

var TestCase = (function () {
    function TestCase(args) {
        this.args = args;
        this.labels = [];
        this.classifications = [];
    }
    TestCase.prototype.getArg = function (ix) {
        return this.args[ix];
    };
    TestCase.prototype.getArgs = function () {
        return this.args;
    };
    TestCase.prototype.getLabels = function () {
        return this.labels;
    };
    TestCase.prototype.getResult = function () {
        return this.result;
    };
    TestCase.prototype.setResult = function (result) {
        this.result = result;
    };
    TestCase.prototype.getClassificationString = function () {
        return this.classifications.join(', ');
    };

    TestCase.prototype.label = function (labelName, showLabel) {
        if (!_.isBoolean(showLabel)) {
            this.labels.push(labelName);
        } else if (showLabel) {
            this.labels.push(labelName);
        }

        return _.isBoolean(showLabel) ? showLabel : true;
    };

    TestCase.prototype.implies = function (implicationResult) {
        if (!implicationResult) {
            throw new ImplicationFailure("Implication Failed");
        }
        return implicationResult;
    };

    TestCase.prototype.classify = function (test, trueLabel, falseLabel) {
        if (test) {
            this.classifications.push(trueLabel);
        } else if (!test && falseLabel) {
            this.classifications.push(falseLabel);
        }

        return test;
    };

    TestCase.prototype.collect = function (value) {
        this.classifications.push(value.toString());
        return value;
    };
    return TestCase;
})();
exports.TestCase = TestCase;

exports.DefaultRunParameters = {
    minTestPasses: 100,
    maxDiscardRatio: 5,
    minSize: 0,
    maxSize: 100,
    explore: false,
    rng: Math.random
};

(function (PropertyOperator) {
    PropertyOperator[PropertyOperator["AND"] = 0] = "AND";
    PropertyOperator[PropertyOperator["OR"] = 1] = "OR";
})(exports.PropertyOperator || (exports.PropertyOperator = {}));
var PropertyOperator = exports.PropertyOperator;

var BaseProperty = (function () {
    function BaseProperty() {
    }
    BaseProperty.prototype.run = function (p) {
        throw new Error("BaseProperty does not implement run method.");
    };

    BaseProperty.prototype.and = function (prop) {
        return new CombinedProperty(0 /* AND */, [this, prop]);
    };

    BaseProperty.prototype.or = function (prop) {
        return new CombinedProperty(1 /* OR */, [this, prop]);
    };

    BaseProperty.prototype.check = function (p) {
        return PropRunner.check(this, p);
    };

    BaseProperty.prototype.checkData = function (p) {
        return PropRunner.checkProp(this, p);
    };
    return BaseProperty;
})();
exports.BaseProperty = BaseProperty;

var CombinedProperty = (function (_super) {
    __extends(CombinedProperty, _super);
    function CombinedProperty(operator, properties) {
        _super.call(this);
        this.operator = operator;
        this.properties = properties;
    }
    CombinedProperty.prototype.combineResults = function (results) {
        switch (this.operator) {
            case 0 /* AND */:
                var status = results.reduce(function (lowest, r) {
                    return r.status < lowest ? r.status : lowest;
                }, 2 /* Passed */);
                return results[0].combine(status, results.slice(1));

            case 1 /* OR */:
                var status = results.reduce(function (highest, r) {
                    return r.status > highest ? r.status : highest;
                }, 0 /* Failed */);
                return results[0].combine(status, results.slice(1));
        }
        ;
    };

    CombinedProperty.prototype.run = function (p) {
        var _this = this;
        var results = this.properties.map(function (prop) {
            return prop.run(p);
        });
        var promised = results.some(function (r) {
            return !(r instanceof Result);
        });

        if (!promised) {
            return this.combineResults(results);
        } else {
            var pResults = results.map(function (r) {
                return r instanceof Result ? Promise.resolve(r) : r;
            });

            return Promise.all(pResults).then(function (res) {
                return _this.combineResults(res);
            });
        }
    };
    return CombinedProperty;
})(BaseProperty);
exports.CombinedProperty = CombinedProperty;

var PropertyGroup = (function (_super) {
    __extends(PropertyGroup, _super);
    function PropertyGroup(name) {
        _super.call(this);
        this.name = name;
        this.properties = {};
    }
    PropertyGroup.prototype.add = function (propName, property) {
        this.properties[propName] = property;
    };

    PropertyGroup.prototype.combineResults = function (results) {
        var status = _.values(results).reduce(function (lowest, r) {
            return r.status < lowest ? r.status : lowest;
        }, 2 /* Passed */);

        return new GroupResult(status, _.values(results).filter(function (r) {
            return r.status == 2 /* Passed */;
        }).length, _.values(results).filter(function (r) {
            return r.status == 1 /* Undecided */;
        }).length, {}, undefined, results);
    };

    PropertyGroup.prototype.run = function (p) {
        var _this = this;
        var results = _.mapValues(this.properties, function (prop) {
            return prop.run(p);
        });
        var promised = _.values(results).some(function (r) {
            return !(r instanceof Result);
        });

        if (!promised) {
            return this.combineResults(results);
        } else {
            var keys = _.keys(results);
            var pResults = _.values(results).map(function (r) {
                return r instanceof Result ? Promise.resolve(r) : r;
            });

            return Promise.all(pResults).then(function (res) {
                return _this.combineResults(_.zipOject(keys, res));
            });
        }
    };
    return PropertyGroup;
})(BaseProperty);
exports.PropertyGroup = PropertyGroup;

/// QUANTIFIED PROPERTIES
var QuantifiedProperty = (function (_super) {
    __extends(QuantifiedProperty, _super);
    function QuantifiedProperty() {
        _super.call(this);
        var args = arguments;

        this.generators = Array.prototype.slice.call(args, 0, args.length - 1);
        this.testFunction = arguments[arguments.length - 1];
    }
    // shrinkTestCase
    //
    // collect all possible shrink candidates from generators, and then
    // aggressively test from smallest to largest
    QuantifiedProperty.prototype.shrinkTestCase = function (testCase) {
        var candidates = this.generators.map(function (gen, ix) {
            return gen.shrink(testCase.getArg(ix));
        });

        if (!candidates.every(function (xs) {
            return xs.length > 0;
        })) {
            // some cases lack candidates, already smallest case
            return testCase;
        }
        ;

        var iterator = candidates.map(function (xs) {
            return 0;
        });

        while (iterator.every(function (cIx, ix) {
            return cIx < candidates[ix].length - 1;
        })) {
            // advance final iterator of product
            iterator[iterator.length - 1]++;

            for (var ix = iterator.length - 1; ix > -1; ix--) {
                if (iterator[ix] == candidates[ix].length) {
                    iterator[ix] = 0;

                    if (ix - 1 > -1) {
                        // advance next
                        iterator[ix - 1]++;
                    }
                }
            }

            var args = iterator.map(function (cIx, ix) {
                return candidates[ix][cIx];
            });
            var nextTestCase = new TestCase(args);

            var nextResult = this.testFunction.apply(nextTestCase, args);
            if (!nextResult) {
                // found failure, return case
                nextTestCase.setResult(nextResult);
                return nextTestCase;
            }
        }
        ;

        // all shrunk cases passed
        return testCase;
    };

    QuantifiedProperty.prototype.promiseShrinkTestCase = function (testCase) {
        var candidates = this.generators.map(function (gen, ix) {
            return gen.shrink(testCase.getArg(ix));
        });

        if (!candidates.every(function (xs) {
            return xs.length > 0;
        })) {
            // some cases lack candidates, already smallest case
            return Promise.resolve(testCase);
        }
        ;

        var iterator = candidates.map(function (xs) {
            return 0;
        });
        var results = [];

        while (iterator.every(function (cIx, ix) {
            return cIx < candidates[ix].length - 1;
        })) {
            // advance final iterator of product
            iterator[iterator.length - 1]++;

            for (var ix = iterator.length - 1; ix > -1; ix--) {
                if (iterator[ix] == candidates[ix].length) {
                    iterator[ix] = 0;

                    if (ix - 1 > -1) {
                        // advance next
                        iterator[ix - 1]++;
                    }
                }
            }

            var args = iterator.map(function (cIx, ix) {
                return candidates[ix][cIx];
            });
            var nextTestCase = new TestCase(args);

            try  {
                var result = this.testFunction.apply(nextTestCase, args);
            } catch (e) {
                if (e.name == "ImplicationFailure") {
                    continue;
                } else {
                    throw e;
                }
            }

            if (typeof result === 'boolean') {
                nextTestCase.setResult(result);
                results.push(Promise.resolve(nextTestCase));
            } else {
                (function () {
                    var promisedCase = nextTestCase;
                    results.push(result.then(function (res) {
                        promisedCase.setResult(res);
                        return promisedCase;
                    }));
                })();
            }
        }
        ;

        // all shrunk cases passed
        return Promise.settle(results).then(function (resultSet) {
            for (var ix = 0; ix < resultSet.length; ix++) {
                var pi = resultSet[ix];

                if (pi.isRejected()) {
                    if (pi._settledValue.name == 'ImplicationFailure') {
                        continue;
                    } else {
                        throw pi._settledValue;
                    }
                } else {
                    var nextTestCase = pi.value();
                }

                if (nextTestCase.getResult()) {
                    continue;
                } else {
                    return nextTestCase;
                }
            }
            ;

            return testCase;
        });
    };

    QuantifiedProperty.prototype.run = function (p) {
        p = p || {};
        _.defaults(p, exports.DefaultRunParameters);

        var stepSize = (p.maxSize - p.minSize) / p.minTestPasses;

        var results = [];
        var passes = 0;
        var discards = 0;
        var currentSize = 0;
        var collectedTestData = {};

        var discardRatioPassed = function () {
            return (discards + passes > p.minTestPasses && discards / p.maxDiscardRatio >= p.minTestPasses);
        };

        while (passes < p.minTestPasses) {
            if (discardRatioPassed()) {
                break;
            }

            var args = this.generators.map(function (gen) {
                return gen.arbitrary(p.rng, currentSize);
            });

            var testCase = new TestCase(args);

            try  {
                var result = this.testFunction.apply(testCase, args);
            } catch (e) {
                if (e.name == "ImplicationFailure") {
                    discards++;
                    currentSize += stepSize;
                    continue;
                } else {
                    throw e;
                }
            }

            if (typeof result !== 'boolean' && result['then']) {
                if (result.isRejected()) {
                    if (result._settledValue.name != 'ImplicationFailure') {
                        return Promise.reject(result._settledValue);
                    }
                }

                return this.finishRunPromised(results, p, currentSize, stepSize);
            } else if (typeof result !== 'boolean') {
                throw new Error("Property not returning boolean or promise.");
            }

            testCase.setResult(result);
            results.push(testCase);

            if (!testCase.getResult()) {
                break;
            } else {
                var classString = testCase.getClassificationString();
                if (classString) {
                    if (_.isUndefined(collectedTestData[classString])) {
                        collectedTestData[classString] = 0;
                    }
                    collectedTestData[classString]++;
                }

                currentSize += stepSize;
                passes++;
            }
        }

        // return result of testing
        if (discardRatioPassed()) {
            return new Result(1 /* Undecided */, passes, discards, collectedTestData);
        } else if (passes < p.minTestPasses) {
            var failCase = _.find(results, function (res) {
                return !res.result;
            });
            var shrunkFailCase = this.shrinkTestCase(failCase);

            return new Result(0 /* Failed */, passes, discards, collectedTestData, shrunkFailCase);
        } else {
            return new Result(2 /* Passed */, passes, discards, collectedTestData);
        }
    };

    QuantifiedProperty.prototype.finishRunPromised = function (tresults, p, size, step) {
        var _this = this;
        var results = tresults.map(Promise.resolve);

        var sprintProgress = results.length;
        var sprintEnd = p.minTestPasses;
        var stepSize = step;
        var currentSize = size;
        var discards = 0;

        var discardRatioPassed = function () {
            return (discards + sprintProgress > p.minTestPasses && discards / p.maxDiscardRatio >= p.minTestPasses);
        };

        var doSprint = function () {
            while (sprintProgress < sprintEnd) {
                if (discardRatioPassed()) {
                    break;
                }

                var args = _this.generators.map(function (gen) {
                    return gen.arbitrary(p.rng, currentSize);
                });

                var testCase = new TestCase(args);

                try  {
                    var result = _this.testFunction.apply(testCase, args);
                } catch (e) {
                    if (e.name == "ImplicationFailure") {
                        discards++;
                        currentSize += stepSize;
                        continue;
                    } else {
                        throw e;
                    }
                }

                if (typeof result === 'boolean') {
                    testCase.setResult(result);
                    results.push(Promise.resolve(testCase));
                } else {
                    (function () {
                        var promisedCase = testCase;
                        results.push(result.then(function (res) {
                            promisedCase.setResult(res);
                            return promisedCase;
                        }));
                    })();
                }
                currentSize += stepSize;
                sprintProgress++;
            }

            return Promise.settle(results).then(function (resultSet) {
                var collectedTestData = {};
                var passes = 0;
                var discardRatioPassed = function () {
                    return (discards + passes > p.minTestPasses && discards / p.maxDiscardRatio >= p.minTestPasses);
                };

                for (var ix = 0; ix < resultSet.length; ix++) {
                    var pi = resultSet[ix];

                    if (pi.isRejected()) {
                        if (pi._settledValue.name == 'ImplicationFailure') {
                            discards++;
                            continue;
                        } else {
                            throw pi._settledValue;
                        }
                    } else {
                        var testCase = pi.value();
                    }

                    if (!testCase.getResult()) {
                        break;
                    } else {
                        var classString = testCase.getClassificationString();
                        if (classString) {
                            if (_.isUndefined(collectedTestData[classString])) {
                                collectedTestData[classString] = 0;
                            }
                            collectedTestData[classString]++;
                        }

                        passes++;
                    }
                }
                ;

                var failCase = _.find(resultSet, function (res) {
                    return res.isRejected() ? false : !res.value().result;
                });

                // return result of testing
                if (discardRatioPassed()) {
                    return new Result(1 /* Undecided */, passes, discards, collectedTestData);
                } else if (failCase) {
                    // promised not shrinking for now
                    // var shrunkFailCase = this.shrinkTestCase(failCase);
                    var pR = _this.promiseShrinkTestCase(failCase.value());
                    return pR.then(function (shrunk) {
                        return new Result(0 /* Failed */, passes, discards, collectedTestData, shrunk);
                    });
                } else if (passes >= p.minTestPasses) {
                    return new Result(2 /* Passed */, passes, discards, collectedTestData);
                } else {
                    sprintEnd += p.minTestPasses;
                    return doSprint();
                }
            });
        };
        return doSprint();
    };
    return QuantifiedProperty;
})(BaseProperty);
exports.QuantifiedProperty = QuantifiedProperty;

var QuantifiedPropertyNoShrink = (function (_super) {
    __extends(QuantifiedPropertyNoShrink, _super);
    function QuantifiedPropertyNoShrink() {
        _super.apply(this, arguments);
    }
    QuantifiedPropertyNoShrink.prototype.shrinkTestCase = function (testCase) {
        return testCase;
    };
    return QuantifiedPropertyNoShrink;
})(QuantifiedProperty);
exports.QuantifiedPropertyNoShrink = QuantifiedPropertyNoShrink;

// SHORTHAND INTERFACE
// late import to prevent circular problems
var Commands = require('./Commands');

function forAll() {
    var args = [];
    for (var _i = 0; _i < (arguments.length - 0); _i++) {
        args[_i] = arguments[_i + 0];
    }
    var P = function (argv) {
        QuantifiedProperty.apply(this, argv);
    };
    P.prototype = QuantifiedProperty.prototype;
    return new P(args);
}
exports.forAll = forAll;

function forAllNoShrink() {
    var args = [];
    for (var _i = 0; _i < (arguments.length - 0); _i++) {
        args[_i] = arguments[_i + 0];
    }
    var P = function (argv) {
        QuantifiedPropertyNoShrink.apply(this, argv);
    };
    P.prototype = QuantifiedPropertyNoShrink.prototype;
    return new P(args);
}
exports.forAllNoShrink = forAllNoShrink;

function all() {
    var properties = [];
    for (var _i = 0; _i < (arguments.length - 0); _i++) {
        properties[_i] = arguments[_i + 0];
    }
    return new CombinedProperty(0 /* AND */, properties);
}
exports.all = all;

function atLeastOne() {
    var properties = [];
    for (var _i = 0; _i < (arguments.length - 0); _i++) {
        properties[_i] = arguments[_i + 0];
    }
    return new CombinedProperty(1 /* OR */, properties);
}
exports.atLeastOne = atLeastOne;

function group(name) {
    return new PropertyGroup(name);
}
exports.group = group;

function commands(commandSpec) {
    return new Commands.CommandsProperty(commandSpec);
}
exports.commands = commands;
