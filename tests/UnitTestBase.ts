///<reference path="../typedefs/node.d.ts"/>

declare var describe, it, before; // Mocha injections.
import PropTests = require('./PropertiesUnitTests');
import GeneratorTests = require('./GeneratorUnitTests');
import PropRunnerTests = require('./PropRunnerUnitTests');
import ExampleReactComponent = require('../examples/ReactComponentUnitTest');

describe("SwiftCheck", () => {
    PropTests;
    GeneratorTests;
    PropRunnerTests;
});
